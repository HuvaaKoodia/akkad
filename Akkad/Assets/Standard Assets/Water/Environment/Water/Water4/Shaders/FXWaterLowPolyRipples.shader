Shader "FX/WaterLowPolyRipples"
{
	Properties
	{
		_Alpha("Alpha", Float) = 1
		_Color("Color", Color) = (1,0,0,1)
		_SpecColor("Specular Material Color", Color) = (1,1,1,1)
		_Shininess("Shininess", Float) = 1.0
		_WaveLength("Wave length", Float) = 0.5
		_WaveHeight("Wave height", Float) = 0.5
		_WaveSpeed("Wave speed", Float) = 1.0
		_RandomHeight("Random height", Float) = 0.5
		_RandomSpeed("Random Speed", Float) = 0.5

		[NoScaleOffset] _RippleMap("Ripple Map", 2D) = "white" {}
		[NoScaleOffset] _RippleMask("Ripple Mask", 2D) = "white" {}
		_RippleRadius("RippleRadius", Range(0,10.5)) = 0.1
	}
		SubShader
		{

			Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" "LightMode" = "ForwardBase" }
			Blend SrcAlpha OneMinusSrcAlpha

			Pass
		{

			CGPROGRAM
	#include "UnityCG.cginc"
	#include "WaterInclude.cginc"

	#pragma vertex vert
	#pragma geometry geom
	#pragma fragment frag

			float rand(float3 co)
			{
				return frac(sin(dot(co.xyz ,float3(12.9898,78.233,45.5432))) * 43758.5453);
			}

			float rand2(float3 co)
			{
				return frac(sin(dot(co.xyz ,float3(19.9128,75.2,34.5122))) * 12765.5213);
			}

			float _Alpha;
			float _WaveLength;
			float _WaveHeight;
			float _WaveSpeed;
			float _RandomHeight;
			float _RandomSpeed;

			uniform float4 _LightColor0;
			uniform float4 _Color;
			uniform float4 _SpecColor;
			uniform float4 _ReflectionColor;
			uniform float _Shininess;
			uniform float4 _DistortParams;

			sampler2D _ReflectionTex;

			//ripples
			uniform float3 _Points[10]; //X,Y,Time
			sampler2D _RippleMap, _RippleMask;
			float4 _RippleMap_ST;
			float _RippleRadius;

			struct v2g
			{
				float4  pos : SV_POSITION;
				float3	norm : NORMAL;
				float2  uv : TEXCOORD0;
			};

			struct g2f
			{
				float4  pos : SV_POSITION;
				float3  norm : NORMAL;
				float2  uv : TEXCOORD0;
				float3 viewDirection : TEXCOORD1;
				float3 diffuseColor : TEXCOORD2;
				float3 specularColor : TEXCOORD3;
			};

			v2g vert(appdata_full v)
			{
				float3 v0 = mul(unity_ObjectToWorld, v.vertex).xyz;

				float phase0 = (_WaveHeight)* sin((_Time[1] * _WaveSpeed) + (v0.x * _WaveLength) + (v0.z * _WaveLength) + rand2(v0.xzz));
				float phase0_1 = (_RandomHeight)*sin(cos(rand(v0.xzz) * _RandomHeight * cos(_Time[1] * _RandomSpeed * sin(rand(v0.xxz)))));

				v0.y += phase0 + phase0_1;

				v.vertex.xyz = mul((float3x3)unity_WorldToObject, v0);

				v2g OUT;
				OUT.pos = v.vertex;
				OUT.norm = v.normal;
				OUT.uv = v.texcoord;
				return OUT;
			}

			[maxvertexcount(3)]
			void geom(triangle v2g IN[3], inout TriangleStream<g2f> triStream)
			{
				float3 v0 = IN[0].pos.xyz;
				float3 v1 = IN[1].pos.xyz;
				float3 v2 = IN[2].pos.xyz;

				float3 centerPos = (v0 + v1 + v2) / 3.0;
				float3 vn = normalize(cross(v1 - v0, v2 - v0));
				float3 worldCenterPos = mul(unity_ObjectToWorld, float4(centerPos, 0.0)).xyz;

				float3 worldNormal = UnityObjectToWorldNormal(vn);
				float3 viewDirection = normalize(_WorldSpaceCameraPos - worldCenterPos);
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
				//float3 lightDirectionFromCenter = normalize(_WorldSpaceLightPos0.xyz - worldCenterPos);
				float attenuation = 1.0;

				//lightDirection = lightDirectionFromCenter;

				float3 ambientLighting = UNITY_LIGHTMODEL_AMBIENT.rgb * _Color.rgb;

				float3 diffuseReflection =
					attenuation * _LightColor0.rgb * _Color.rgb
					* max(0.0, dot(worldNormal, lightDirection));

				float3 specularReflection;
				if (dot(worldNormal, lightDirection) < 0.0)
				{
					specularReflection = float3(0.0, 0.0, 0.0);
				}
				else
				{
					specularReflection = attenuation * _LightColor0.rgb
						* _SpecColor.rgb * pow(max(0.0, dot(
							reflect(-lightDirection, worldNormal),
							viewDirection)), _Shininess);
				}

				g2f OUT;
				OUT.pos = mul(UNITY_MATRIX_MVP, IN[0].pos);
				OUT.norm = vn;
				OUT.uv = IN[0].uv;
				OUT.diffuseColor = ambientLighting + diffuseReflection;
				OUT.specularColor = specularReflection;
				OUT.viewDirection = viewDirection;
				triStream.Append(OUT);
				
				OUT.pos = mul(UNITY_MATRIX_MVP, IN[1].pos);
				OUT.norm = vn;
				OUT.uv = IN[1].uv;
				OUT.diffuseColor = ambientLighting + diffuseReflection;
				OUT.specularColor = specularReflection;
				OUT.viewDirection = viewDirection;
				triStream.Append(OUT);

				OUT.pos = mul(UNITY_MATRIX_MVP, IN[2].pos);
				OUT.norm = vn;
				OUT.uv = IN[2].uv;
				OUT.diffuseColor = ambientLighting + diffuseReflection;
				OUT.specularColor = specularReflection;
				OUT.viewDirection = viewDirection;
				triStream.Append(OUT);
			}

			half4 frag(g2f IN) : COLOR
			{
				half4 baseColor = half4(IN.diffuseColor,0);
				baseColor = half4(baseColor + IN.specularColor, _Alpha);

				//ripples
				int times = 0;
				half3 totalRippleColor = baseColor;
				for (int p = 0; p < 10; p++)
				{
					float time = _Points[p].z;
					if (times > 1) break;
					if (time < 0) continue;

					float x = _Points[p].x;
					float y = _Points[p].y;

					float xPosMin = x - _RippleRadius * time;
					float yPosMin = y - _RippleRadius * time;
					float xPosMax = x + _RippleRadius * time;
					float yPosMax = y + _RippleRadius * time;

					float wxPos = clamp(IN.uv.x, xPosMin, xPosMax);
					float wyPos = clamp(IN.uv.y, yPosMin, yPosMax);
					float uvxPos = (wxPos - xPosMin) / (xPosMax - xPosMin);
					float uvyPos = (wyPos - yPosMin) / (yPosMax - yPosMin);

					half3 rippleColor = tex2D(_RippleMap, float2(uvxPos, uvyPos));
					half3 maskColor = tex2D(_RippleMask, float2(uvxPos, uvyPos));

					float ripplePercent = 0.5 * ((1 - clamp(time, 0.7, 1.0)) / 0.3);
					float baseColorPercent = 1 - ripplePercent;

					if (maskColor.r == 1)
					{
						times++;
						totalRippleColor = baseColor * baseColorPercent + rippleColor * ripplePercent;
					}
				}

				if (times == 1)
					baseColor.xyz = totalRippleColor;

				return baseColor;
			}
			ENDCG
		}
		}
}