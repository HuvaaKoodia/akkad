// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "FX/WaterReflections" {
	Properties{
		//reflection
		_ReflectionTex("Reflections", 2D) = "white" {}
		_ReflectionColor("Reflection color", COLOR) = (.54, .95, .99, 0.5)
		_Alpha("Alpha", Range(0,1)) = 1

		//waves
		_WaveLength("Wave length", Float) = 0.5
		_WaveHeight("Wave height", Float) = 0.5
		_WaveSpeed("Wave speed", Float) = 1.0
		_RandomHeight("Random height", Float) = 0.5
		_RandomSpeed("Random Speed", Float) = 0.5
	}

	CGINCLUDE

#include "UnityCG.cginc"
#include "WaterInclude.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			float3 normal : NORMAL;
		};

		// interpolator structs

		struct v2f
		{
			float4 pos : SV_POSITION;
			float4 normalInterpolator : TEXCOORD0;
			float4 viewInterpolator : TEXCOORD1;
			
			float4 screenPos : TEXCOORD3;
			float4 grabPassPos : TEXCOORD4;
			UNITY_FOG_COORDS(5)
		};

		sampler2D _ReflectionTex;
		uniform float4 _ReflectionColor;
		uniform float _Alpha;

		float _WaveLength;
		float _WaveHeight;
		float _WaveSpeed;
		float _RandomHeight;
		float _RandomSpeed;

		float rand(float3 co)
		{
			return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
		}

		float rand2(float3 co)
		{
			return frac(sin(dot(co.xyz, float3(19.9128, 75.2, 34.5122))) * 12765.5213);
		}

		// shortcuts
#define VERTEX_WORLD_NORMAL i.normalInterpolator.xyz
#define NORMAL_DISPLACEMENT_PER_VERTEX _InvFadeParemeter.z

//
// HQ VERSION
//

		v2f vert(appdata_full v)
		{
			v2f o;

			half3 worldSpaceVertex = mul(unity_ObjectToWorld, (v.vertex)).xyz;

			//move waves and such
			float3 v0 = mul(unity_ObjectToWorld, v.vertex).xyz;

			float phase0 = (_WaveHeight)* sin((_Time[1] * _WaveSpeed) + (v0.x * _WaveLength) + (v0.z * _WaveLength) + rand2(v0.xzz));
			float phase0_1 = (_RandomHeight)*sin(cos(rand(v0.xzz) * _RandomHeight * cos(_Time[1] * _RandomSpeed * sin(rand(v0.xxz)))));

			v0.y += phase0 + phase0_1;

			v.vertex.xyz = mul((float3x3)unity_WorldToObject, v0);

			// one can also use worldSpaceVertex.xz here (speed!), albeit it'll end up a little skewed
			half2 tileableUv = mul(unity_ObjectToWorld, (v.vertex)).xz;

			o.viewInterpolator.xyz = worldSpaceVertex - _WorldSpaceCameraPos;

			o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

			ComputeScreenAndGrabPassPos(o.pos, o.screenPos, o.grabPassPos);

			o.normalInterpolator.xyz = float3(0, 0, 0);

			o.viewInterpolator.w = 1;
			o.normalInterpolator.w = 1;

			UNITY_TRANSFER_FOG(o, o.pos);
			return o;
		}

		half4 frag(v2f i) : SV_Target
		{
			half3 worldNormal = i.normalInterpolator;
			half3 viewVector = normalize(i.viewInterpolator.xyz);

			half4 distortOffset = half4(worldNormal.xz * 10.0, 0, 0);
			half4 screenWithOffset = i.screenPos + distortOffset;

			half4 rtReflections = tex2Dproj(_ReflectionTex, UNITY_PROJ_COORD(screenWithOffset));

			// base, depth & reflection colors
			half4 reflectionColor = lerp(rtReflections,_ReflectionColor,_ReflectionColor.a);

			reflectionColor.a = _Alpha;
			UNITY_APPLY_FOG(i.fogCoord, baseColor);
			return reflectionColor;
		}

			ENDCG

			Subshader
		{
			Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }

				Lod 500
				ColorMask RGB

				GrabPass{ "_RefractionTex" }

				Pass{
						Blend SrcAlpha OneMinusSrcAlpha
						ZTest LEqual
						ZWrite Off
						Cull Back

						CGPROGRAM

						#pragma target 3.0

						#pragma vertex vert
						#pragma fragment frag
						#pragma multi_compile_fog

						#pragma multi_compile WATER_VERTEX_DISPLACEMENT_ON WATER_VERTEX_DISPLACEMENT_OFF
						#pragma multi_compile WATER_EDGEBLEND_ON WATER_EDGEBLEND_OFF
						#pragma multi_compile WATER_REFLECTIVE WATER_SIMPLE

						ENDCG
			}
		}

		Fallback "Transparent/Diffuse"
}
