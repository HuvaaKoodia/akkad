﻿using System;
using UnityEngine;

public class WaterRippleAffector : MonoBehaviour
{
    #region vars
    public Rigidbody Rigidbody;
    public float RaycastDistance = 1f;
    public bool MoveRayPositionBasedOnVelocity = false;
    public float MoveRayDistanceMax = 0.1f;
    public bool Visualize = true;
    public WaterRippleFx FX;
    public Vector2 TriggerDelay = new Vector2(0.1f, 0.2f);
    public Vector2 Duration = new Vector2(0.8f, 1f);
    public AudioPlayer WaterAudio;

    private float canTriggerTimer = 0;
    private int layerMask;
    private bool canTrigger = true;
    #endregion
    #region init
    private void Start()
    {
        layerMask = 1 << LayerMask.NameToLayer("Water");
    }
    #endregion
    #region logic
    private void Update()
    {
        if (Rigidbody.velocity.magnitude > 0.1 && canTriggerTimer < Time.time)
        {
            UpdateTriggerDelay();
            canTrigger = true;
        }

        RaycastHit hitInfo;
        Vector3 rayPosition = transform.position;
        if (MoveRayPositionBasedOnVelocity)
        {
            Vector3 velocity = Rigidbody.velocity;
            velocity.y = 0;
            rayPosition += velocity.normalized * MoveRayDistanceMax;
        }
        if (Physics.Raycast(rayPosition, Vector3.down, out hitInfo, RaycastDistance, layerMask, QueryTriggerInteraction.Collide))
        {
            if (canTrigger)
            {
                canTrigger = false;
                if (!FX) FX = hitInfo.collider.GetComponent<WaterRippleFx>();
                UpdateTriggerDelay();
                FX.StartRipple(hitInfo.textureCoord.x, hitInfo.textureCoord.y, Helpers.Rand(Duration.x, Duration.y));

                if (WaterAudio) WaterAudio.Play3DSound(0);
            }
#if UNITY_EDITOR
            if (Visualize) Debug.DrawRay(transform.position, Vector3.down * RaycastDistance, Color.green);
#endif
        }
        else
        {
            canTrigger = true;
#if UNITY_EDITOR
            if (Visualize) Debug.DrawRay(transform.position, Vector3.down * RaycastDistance, Color.red);
#endif
        }
    }

    private void UpdateTriggerDelay()
    {
        canTriggerTimer = Time.time + Helpers.Rand(TriggerDelay.x, TriggerDelay.y);
    }
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
