﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Manages the water ripple effect.
/// 
/// Setup:
/// - Create a gameobject with a mesh and a mesh collider (non convex, not a trigger)
/// - Assign it to the "Water" layer
/// - Assign a material with the "WaterRipples" shader to the mesh
/// - Add this script to the gameobject
/// - Add the "WaterRippleAffector" script to a moving object (player for instance)
/// - ?????
/// - Should work!
/// </summary>
public class WaterRippleFx : MonoBehaviour
{
    #region vars
    public Renderer Renderer;
    [Tooltip("Based on the width of the mesh bounds. Use uniform meshes for most predictable results.")]
    public float RippleRadiusWorldUnits = 1;
    private int maxRippleCount = 5;
    private IEnumerator[] coroutines;
    private Vector4[] values;
    private int rippleIndex = 0;
    private MaterialPropertyBlock block;
    #endregion
    #region init
    private void Start()
    {
        maxRippleCount = 10;
        block = new MaterialPropertyBlock();

        coroutines = new IEnumerator[maxRippleCount];
        values = new Vector4[maxRippleCount];

        for (int i = 0; i < maxRippleCount; i++)
        {
            values[i] = new Vector3(0, 0, -1);
        }

        float rippleRadius = RippleRadiusWorldUnits / Renderer.bounds.max.x;

        block.SetFloat("_RippleRadius", rippleRadius);
        block.SetVectorArray("_Points", values);
        Renderer.SetPropertyBlock(block);
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    public void StartRipple(float x, float y, float duration)
    {
        if (coroutines[rippleIndex] != null) return;
        //StopCoroutine(coroutines[rippleCount]);
        coroutines[rippleIndex] = RippleCoroutine(rippleIndex, x, y, duration);

        StartCoroutine(coroutines[rippleIndex]);

        rippleIndex++;
        if (rippleIndex == maxRippleCount) rippleIndex = 0;
    }
    #endregion
    #region private interface

    private IEnumerator RippleCoroutine(int index, float posX, float posY, float duration)
    {
        float time = 0;
        values[index].x = posX;
        values[index].y = posY;

        while (time < 1)
        {
            time += Time.deltaTime / duration;
            if (time > 1) time = 1;
            values[index].z = time;
            block.SetVectorArray("_Points", values);
            Renderer.SetPropertyBlock(block);
            yield return null;
        }

        values[index].z = -1;
        block.SetVectorArray("_Points", values);
        Renderer.SetPropertyBlock(block);
        coroutines[index] = null;
    }
    #endregion
    #region events
    /*private void OnTriggerEnter(Collider collider)
    {
        StartRipple(0.5f,0.5f);
    }*/
    #endregion
}
