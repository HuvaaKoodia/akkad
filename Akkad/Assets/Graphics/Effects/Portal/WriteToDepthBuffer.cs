﻿using UnityEngine;

public class WriteToDepthBuffer : MonoBehaviour
{
    void Start ()
    {
        GetComponent<Camera>().depthTextureMode = DepthTextureMode.DepthNormals;
    }
}