﻿Shader "Custom/IlluminatedRimShaderScrolling" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}
		_ScrollNormal("Scrolling Normal", 2D) = "white" {}
		_ScrollNormal2("Scrolling Normal 2", 2D) = "white" {}
		_EmissionColor("Emission Color", Color) = (1,1,1,1)
		_Speed("Speed", float) = 10
		_Border("Border", Range(-1,1)) = 1
		_Ramp("Ramp", 2D) = "white" {}
	}
		SubShader{
			Tags { "RenderType" = "Transparent" "Queue" = "Transparent+10" }
			LOD 200
			Cull Off
			CGPROGRAM
#include "UnityCG.cginc"
			// Physically based Standard lighting model, and enable shadows on all light types
#pragma surface surf Lambert alpha
			// Use shader model 3.0 target, to get nicer looking lighting		
#pragma target 3.0

			sampler2D _MainTex;
			sampler2D _Ramp;
			sampler2D _ScrollNormal;
			fixed4 _ScrollNormal_TexelSize;
			sampler2D _ScrollNormal2;
			fixed4 _ScrollNormal2_TexelSize;

			float _Speed;
			float _Border;
			fixed4 _Color;
			fixed4 _EmissionColor;

			struct Input {
				float2 uv_MainTex;
				float2 uv_ScrollNormal;
				float2 uv_ScrollNormal2;
				float3 viewDir;
			};

			void surf(Input IN, inout SurfaceOutput o)
			{
				// Albedo comes from a texture tinted by color
				float speedX = fmod(_Time * _Speed, _ScrollNormal_TexelSize.z);
				float speedY = fmod(_Time * _Speed, _ScrollNormal_TexelSize.w);
				fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;

				//rim light
				float viewDot = dot(o.Normal, IN.viewDir);
				c *= tex2D(_Ramp, float2(viewDot,0));

				o.Albedo = c.rgb;
				o.Alpha = c.a;
				o.Emission = _EmissionColor;

				float x = IN.uv_ScrollNormal.x - 0.5f, y = IN.uv_ScrollNormal.y - 0.5f;
				float distance = (sqrt(x * x + y * y) * 2);
				float max = _Border, min = 1- max;

				if (distance > max)
				{
					distance = (distance - max) / min;
				}
				else distance = 0;

				fixed4 n1 = tex2D(_ScrollNormal, float2(IN.uv_ScrollNormal.x + 1.2 + speedX, IN.uv_ScrollNormal.y- 1 + speedY));
				fixed4 n2 = tex2D(_ScrollNormal2, float2(IN.uv_ScrollNormal2.x - speedX, IN.uv_ScrollNormal2.y - speedY));
				o.Normal = UnpackNormal((n1 * 0.5 + n2 * 0.5)  * distance);
				
			}
			ENDCG
		}
			FallBack "Diffuse"
}
