[
//Intro
{
	"ID": "AbzuIntroText",
    "CharacterID": "Player",
	"Text": "What the..?",
	"Link": "AbzuIntroText0.1"
},
{
	"ID": "AbzuIntroText0.1",
    "CharacterID": "Player",
	"Text": "Where am i?",
	"Link": "AbzuIntroText0.2"
},
{
	"ID": "AbzuIntroText0.2",
    "CharacterID": "Lvl1Ancient",
	"Text": "My my, it seems I have a guest, it's been a while... I welcome you to the tower, child. My name is ABZU, Guardian of the lowest floor. And what is your name, child?",
	"Link": "AbzuIntroText0.3"
},
{
	"ID": "AbzuIntroText0.3",
    "CharacterID": "Player",
	"Text": "My... my name is James�",
	"Link": "AbzuIntroText0.4"
},
{
	"ID": "AbzuIntroText0.4",
    "CharacterID": "Lvl1Ancient",
	"Text": "My, that's an easy name to call. Good. May I ask how you entered this tower?",
	"Link": "AbzuIntroText0.5"
},
{
	"ID": "AbzuIntroText0.5",
    "CharacterID": "Player",
	"Text": "Ah! I... I was just heading home, and the last thing I remember was running to push a baby's cart away from an incoming car that was going straight at it...",
	"Link": "AbzuIntroText0.6"
},
{
	"ID": "AbzuIntroText0.6",
    "CharacterID": "Lvl1Ancient",
	"Text": "So� you are the victim of an accident, someone who's got his life taken away from him too soon. I see, there could be no other explanation for you to have entered this tower, as always.",
	"Link": "AbzuIntroText0.7"
},
{
	"ID": "AbzuIntroText0.7",
    "CharacterID": "Player",
	"Text": "Wait� I died? Then why the hell am I here? Please tell me what's going on.",
	"Link": "AbzuIntroText0.8"
},
{
	"ID": "AbzuIntroText0.8",
    "CharacterID": "Lvl1Ancient",
	"Text": "If you wish to have your answer, if you wish to know what happens next, head to the top of this tower. All the universe's wisdom and knowledge... has been placed there. This tower is such a place. However, climbing this tower is a long, arduous journey. Child.",
	"Link": "AbzuIntroText0.9"
},
{
	"ID": "AbzuIntroText0.9",
    "CharacterID": "Player",
	"Text": "Hum, so this is a tower, and if i want answers i have to climb it? Where's the stairs?",
	"Link": "AbzuIntroText0.91"
},
{
	"ID": "AbzuIntroText0.91",
    "CharacterID": "Lvl1Ancient",
	"Text": "There are no stairs, there is no need for such mundane things here. This place doesn't function in the same way as your world. For you to climb it, you will need to complete the challenges presented to you on each floor.",
	"Link": "AbzuIntroText0.92"
},
{
	"ID": "AbzuIntroText0.92",
    "CharacterID": "Player",
	"Text": "Why challenges, and what kind of?",
	"Link": "AbzuIntroText0.93"
},
{
	"ID": "AbzuIntroText0.93",
    "CharacterID": "Lvl1Ancient",
	"Text": "To determine whether or not you are worthy to go to the next floor, each floor holds a test. If you pass the test, you can proceed to the next floor. ",
	"Link": "AbzuIntroText0.94"
},
{
	"ID": "AbzuIntroText0.94",
    "CharacterID": "Lvl1Ancient",
	"Text": "The test is determined by the guardians of each floor, and of course, difficulty increases with each level� Well, since this is just the very first level, I will give you a very small test, just to see how physically apt you are�",
	"Link": "AbzuIntroText0.95"
},
{
	"ID": "AbzuIntroText0.95",
    "CharacterID": "Lvl1Ancient",
	"Text": "All you have to do, is gather the floating light orbs that I placed around. If you manage to bring me back 10 of them without dying, i shall open the door to the next one.",
},

	//OrbMissionState
{
	"ID": "AbzuMissionText1",
    "CharacterID": "Lvl1Ancient",
	"Text": "All you have to do, is gather the light orbs that float around. If you manage to bring me back 10 of them without dying, i shall open the door to the next one.",
},

	//AfterOrbs
{
	"ID": "AbzuMissionEnd1",
    "CharacterID": "Lvl1Ancient",
	"Text": "So, apparently you were able to get all the orbs. I suppose that�s normal seeing how young you are. Well then, you appear to be physically apt for the remaining tests, and therefore I approve your passage. Please enter the portal.",
},
]