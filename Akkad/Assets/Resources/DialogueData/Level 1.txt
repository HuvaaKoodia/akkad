[
//Intro
{
	"ID": "Lvl1_IntroText",
    "CharacterID": "Player",
	"Text": "What the...",
	"Link": "Lvl1_IntroText2",
},
{
	"ID": "Lvl1_IntroText2",
    "CharacterID": "Player",
	"Text": "Where am I?",
},
{
	"ID": "AbzuIntroText",
    "CharacterID": "Lvl1Ancient",
	"Text": "My my, it seems I have a guest... I welcome you to the tower, child. I am Abzu, Guardian of this floor. And what is your name?",
	"Link": "AbzuIntroText2"
},
{
	"ID": "AbzuIntroText2",
    "CharacterID": "Player",
	"Text": "My... my name is James�",
	"Link": "AbzuIntroText3"
},
{
	"ID": "AbzuIntroText3",
    "CharacterID": "Lvl1Ancient",
	"Text": "That is an easy name to call, good. May I ask how you entered this tower?",
	"Link": "AbzuIntroText4"
},
{
	"ID": "AbzuIntroText4",
    "CharacterID": "Player",
	"Text": "Ah! I... I was just walking home and the last thing I remember seeing... is the headlights of an incoming car!",
	"Link": "AbzuIntroText5"
},
{
	"ID": "AbzuIntroText5",
    "CharacterID": "Lvl1Ancient",
	"Text": "So� you are the victim of an accident, someone whose life was taken away from him too soon. To be honest there could been no other explanation for you to have entered this tower.",
	"Link": "AbzuIntroText6"
},
{
	"ID": "AbzuIntroText6",
    "CharacterID": "Player",
	"Text": "Wait! I died? Then where the hell am I?",
	"Link": "AbzuIntroText7"
},
{
	"ID": "AbzuIntroText7",
    "CharacterID": "Lvl1Ancient",
	"Text": "Hell this is not, that much I can tell you. If you wish to have more answers you will have to reach the top of this tower.",
	"Link": "AbzuIntroText7b"
},
{
	"ID": "AbzuIntroText7b",
    "CharacterID": "Lvl1Ancient",
	"Text": "All the wisdom and knowledge of the universe... has been placed there. However, the journey is long and arduous.",
	"Link": "AbzuIntroText8"
},
{
	"ID": "AbzuIntroText8",
    "CharacterID": "Player",
	"Text": "A tower, huh. So how do I get to the top? Where's the stairs?",
	"Link": "AbzuIntroText9"
},
{
	"ID": "AbzuIntroText9",
    "CharacterID": "Lvl1Ancient",
	"Text": "This world doesn't function in the same way as yours. To climb this tower you will need to complete a series of challenges presented to you on each floor to prove your worth.",
	"Link": "AbzuIntroText10"
},

{
	"ID": "AbzuIntroText10",
    "CharacterID": "Lvl1Ancient",
	"Text": "Since this is the first floor, I will only give you a small test to see how physically apt you are. All you have to do is to gather some light orbs. If you manage to bring me 5 of them, I shall open the door to the next floor.",
	"Link": "AbzuIntroText12"
},

{
	"ID": "AbzuIntroText12",
    "CharacterID": "Player",
	"Text": "...I will try.",
},

//OrbMissionState
{
	"ID": "AbzuMissionText",
    "CharacterID": "Lvl1Ancient",
	"Text": "All you have to do, is gather 5 light orbs floating around. Only then shall I open the door to the next floor.",
},


//AfterOrbs
{
	"ID": "AbzuMissionEnd",
    "CharacterID": "Lvl1Ancient",
	"Text": "So, you were capable of collecting enough orbs. I suppose that was expected seeing how young you are. Well then, I approve of your passage. You may enter the portal.",
},
]