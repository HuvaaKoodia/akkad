﻿using UnityEngine;

namespace Cameras.Rigs
{
    /// <summary>
    /// A camera rig that orbits the camera around an object on the y axis.
    /// Has build in object avoidance and other niceties like orbiting around a spline.
    /// </summary>
    public class TargetOrbitCamera : MonoBehaviour
    {
        #region variables
        [System.Serializable]
        public class ProximityPitchIncreaseStats
        {
            public bool Active = true, Visualize = true;
            public float MaxAngle = 45f;
            public float MinDistance = 10f, MaxDistance = 40f;
        }
        [System.Serializable]
        public class HorizontalObstacleAvoidanceStats
        {
            public bool Active = true, Visualize = true;
            public float MaxAngle = 45f;
            public float RaycastFrequency = 5f;
            public float RaycastDistance = 10f;
            public float AngleAmountMulti = 2f;
            public float AngleChangeSpeed = 2f;
            public int Mask { get; set; }
        }
        [System.Serializable]
        public class VerticalObstacleAvoidanceStats
        {
            public bool Active = true, Visualize = true;
            public float MaxAngle = 45f;
            public float RaycastFrequency = 5f;
            public float RaycastDistance = 10f;
            public float AngleChangeSpeed = 2f;
            public int Mask { get; set; }
        }
        [System.Serializable]
        public class DirectionAvoidanceStats
        {
            public bool Active = true, Visualize = true;
            public float Speed = 3f, MinDistance = 1f;
            public int Mask { get; set; }
        }
        [System.Serializable]
        public class GroundAvoidanceStats
        {
            public bool Active = true, Visualize = true;
            public float RaycastHeight = 5f, VerticalOffset = 0.1f;
            public int Mask { get; set; }
        }

        public Transform CameraPosition;
        public Camera Camera;
        public Transform FollowTarget;
        public Transform LookTarget;
        public float Distance = 6;
        public float VerticalOffset = 0;
        public float MaxLookHeightDifference;
        public HorizontalObstacleAvoidanceStats HorizontalObstacleAvoidance;
        public VerticalObstacleAvoidanceStats VerticalObstacleAvoidance;
        public DirectionAvoidanceStats DirectionAvoidance;
        public GroundAvoidanceStats GroundAvoidance;
        public ProximityPitchIncreaseStats ProximityPitchIncrease;

        private float horizontalAvoidanceAngle = 0;
        private float verticalAvoidanceAngle = 0;
        private float horizontalAvoidanceAngleOld = 0;
        private float currentDistance = 0;
        private Ray ray;
        private RaycastHit hit;
        #endregion

        #region initialization

        private void Awake()
        {
            HorizontalObstacleAvoidance.Mask = LayerMasks.ObstacleBlockCamera;
            VerticalObstacleAvoidance.Mask = LayerMasks.ObstacleBlockCamera;
            DirectionAvoidance.Mask = LayerMasks.ObstacleBlockCamera | LayerMasks.Water;
            GroundAvoidance.Mask = LayerMasks.Ground;

            //cleaning up editor values
            if (VerticalObstacleAvoidance.RaycastDistance < Distance) VerticalObstacleAvoidance.RaycastDistance = Distance;
        }

        private void OnEnable()
        {
            currentDistance = Distance;
        }

        #endregion

        #region logic

        public void FixedUpdate()
        {
            float pitch = 0;
            float yaw = 0;

            //calculate camera orbit around position
            Vector3 OrbitOrigin = LookTarget.position;

            //flatten the look target angle if looking downwards (moves towards the player when looking up)
            {
                float heightDifference = Vector3.Dot(FollowTarget.To(LookTarget), FollowTarget.up);

                if (heightDifference < MaxLookHeightDifference)
                {
                    heightDifference = MaxLookHeightDifference;

                    OrbitOrigin = LookTarget.position + FollowTarget.up * -heightDifference;
                }
            }

            //calculate follow target position based on vertical offset.
            Vector3 verticalOffset = FollowTarget.up * VerticalOffset;
            Vector3 followTargetPosition = FollowTarget.position + verticalOffset;

            //calculate basic camera position
            Vector3 originDirection = OrbitOrigin.To(followTargetPosition);
            float distanceToOrigin = originDirection.magnitude;
            originDirection.Normalize();

            Vector3 cameraPosition = followTargetPosition + originDirection * Distance + verticalOffset;

            bool insideWall = false;
            {
                //check to see if target is inside wall
                ray = new Ray(followTargetPosition, originDirection);
                if (Physics.SphereCast(ray, 0.4f, Distance, HorizontalObstacleAvoidance.Mask))
                {
                    insideWall = true;
                }
            }
            insideWall = false;

            //avoid obstacles left and right

            if (!insideWall && HorizontalObstacleAvoidance.Active)
            {
                cameraPosition = followTargetPosition + Quaternion.AngleAxis(horizontalAvoidanceAngleOld, FollowTarget.up) * (originDirection * Distance);

                var horizontalAvoidanceStartPosition = followTargetPosition + originDirection * Distance;

                Vector3 checkDirection = horizontalAvoidanceStartPosition.To(followTargetPosition);
                checkDirection = Vector3.ProjectOnPlane(checkDirection, FollowTarget.up);

                float rayDistance = 0, largestAngle = 0, smallestAngle = 0;
                float angleHalf = HorizontalObstacleAvoidance.MaxAngle * 0.5f;
                //shoot multiple rays in a horizontal fan to check for obstacles
                for (float angle = -angleHalf; angle < angleHalf; angle += HorizontalObstacleAvoidance.RaycastFrequency)
                {
                    ray = new Ray(horizontalAvoidanceStartPosition, Quaternion.AngleAxis(angle, FollowTarget.up) * checkDirection);

                    //set distance to always reach the camera position
                    rayDistance = HorizontalObstacleAvoidance.RaycastDistance / Mathf.Cos(angle * Mathf.Deg2Rad);

                    if (Physics.Raycast(ray, out hit, rayDistance, HorizontalObstacleAvoidance.Mask))
                    {
#if UNITY_EDITOR
                        if (HorizontalObstacleAvoidance.Visualize) Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);
#endif
                        if (angle < 0 && angle < smallestAngle)
                        {
                            smallestAngle = angle;
                        }
                        else if (angle > 0 && angle > largestAngle)
                        {
                            largestAngle = angle;
                        }
                    }
#if UNITY_EDITOR
                    else
                    {
                        if (HorizontalObstacleAvoidance.Visualize) Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.green);
                    }
#endif
                }

                //move camera away from obstacles
                if (smallestAngle == 0 || largestAngle == 0)
                {
                    float totalAngle = (smallestAngle + largestAngle) * HorizontalObstacleAvoidance.AngleAmountMulti;
                    horizontalAvoidanceAngle = Mathf.Lerp(horizontalAvoidanceAngle, totalAngle, HorizontalObstacleAvoidance.AngleChangeSpeed * Time.deltaTime);

                    yaw = horizontalAvoidanceAngle;
                    horizontalAvoidanceAngleOld = horizontalAvoidanceAngle;
                }
            }

            //look up when close to the follow target
            if (ProximityPitchIncrease.Active)
            {
                if (distanceToOrigin < ProximityPitchIncrease.MaxDistance)
                {
                    pitch = -(1f - (Mathf.Max(ProximityPitchIncrease.MinDistance, distanceToOrigin) / ProximityPitchIncrease.MaxDistance)) * ProximityPitchIncrease.MaxAngle;
                }
            }

            float hitDistance = Distance;

            //check for obstacles behind the camera and move vertically to avoid them
            if (VerticalObstacleAvoidance.Active)
            {
                Vector3 checkDirection = followTargetPosition.To(cameraPosition);
                Vector3 pitchAxis = Vector3.Cross(checkDirection, FollowTarget.up);

                float rayDistance = VerticalObstacleAvoidance.RaycastDistance;
                float freeAngle = 0;

                for (float angle = 0; angle < VerticalObstacleAvoidance.MaxAngle; angle += VerticalObstacleAvoidance.RaycastFrequency)
                {
                    ray = new Ray(followTargetPosition, Quaternion.AngleAxis(angle, pitchAxis) * checkDirection);

                    if (Physics.Raycast(ray, out hit, rayDistance, VerticalObstacleAvoidance.Mask))
                    {
#if UNITY_EDITOR
                        if (VerticalObstacleAvoidance.Visualize) Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.red);
#endif
                        if (hit.distance < hitDistance) hitDistance = hit.distance;

                        //set distance to always reach the camera position
                        rayDistance = VerticalObstacleAvoidance.RaycastDistance / Mathf.Cos(angle * Mathf.Deg2Rad);
                        continue;
                    }
#if UNITY_EDITOR
                    if (VerticalObstacleAvoidance.Visualize) Debug.DrawRay(ray.origin, ray.direction * rayDistance, Color.green);
#endif
                    hitDistance = Distance;
                    freeAngle = angle;
                    break;
                }
                verticalAvoidanceAngle = Mathf.Lerp(verticalAvoidanceAngle, freeAngle, HorizontalObstacleAvoidance.AngleChangeSpeed * Time.deltaTime);
                if (freeAngle != 0)
                {
                    pitch = verticalAvoidanceAngle;
                }
                else
                {
                    pitch += verticalAvoidanceAngle;
                }
            }

            //set new camera position
            {
                Vector3 pitchAxis = Vector3.Cross(originDirection, FollowTarget.up);
                Vector3 cameraDirection = Quaternion.AngleAxis(pitch, pitchAxis) * Quaternion.AngleAxis(yaw, FollowTarget.up) * originDirection;

                cameraPosition = followTargetPosition + cameraDirection * Distance;

                //ground avoidance
                bool hitGround = false;
                if (GroundAvoidance.Active)
                {
                    float height = GroundAvoidance.RaycastHeight;
                    Vector3 rayPosition = cameraPosition + FollowTarget.up * height;
                    float rayDistance = height + GroundAvoidance.VerticalOffset;
                    ray = new Ray(rayPosition, -FollowTarget.up);

                   
                    //shoot ray down from a high point to see if the camera position is under ground.
                    if (Physics.Raycast(ray, out hit, rayDistance, GroundAvoidance.Mask))
                    {
                        cameraPosition = rayPosition + ray.direction * (hit.distance - GroundAvoidance.VerticalOffset);
                        hitGround = true;
                    }
#if UNITY_EDITOR
                    if (GroundAvoidance.Visualize) ray.Draw(rayDistance, hitGround ? Color.red : Color.magenta);
#endif
                }

                if (DirectionAvoidance.Active && !hitGround)
                {
                    //basic direction avoidance
                    float rayDistance = VerticalObstacleAvoidance.RaycastDistance;
                    Vector3 rayDirection = followTargetPosition.To(cameraPosition);
                    ray = new Ray(followTargetPosition, rayDirection);

                    float directionCastRadius = Camera.nearClipPlane + 0.1f;
                    hitDistance = 0;

                    //shoot a sphere cast first and a raycast just in case
                    if (Physics.SphereCast(ray, directionCastRadius, out hit, rayDistance, DirectionAvoidance.Mask))
                    {
                        if (hit.distance < Distance) hitDistance = hit.distance;
                    }
                    else if (Physics.Raycast(ray, out hit, rayDistance, DirectionAvoidance.Mask))
                    {
                        if (hit.distance < Distance) hitDistance = hit.distance;
                    }
#if UNITY_EDITOR
                    if (DirectionAvoidance.Visualize) ray.Draw(rayDistance, Color.magenta);
#endif

                    if (hitDistance != 0)
                    {
                        //move immediately if hit something
                        currentDistance = Mathf.Max(DirectionAvoidance.MinDistance, hitDistance);
                    }
                    else
                    {
                        //move smoothly back to normal position
                        currentDistance = Mathf.Lerp(currentDistance, Distance, Time.deltaTime * DirectionAvoidance.Speed);
                    }

                    cameraPosition = followTargetPosition + ray.direction * currentDistance;
                }

                CameraPosition.position = cameraPosition;
            }
        }
        #endregion
    }
}