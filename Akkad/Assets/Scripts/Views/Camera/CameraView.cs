using System.Collections;
using UnityEngine;

namespace Cameras
{

    /// <summary>
    /// Contains values used by the camera shake methods.
    /// </summary>
    [System.Serializable]
    public class CameraShakeStats
    {
        public float MaxDistance = 45f;
        public float Force = 0.7f;
        public float Duration = 0.3f;
        public AnimationCurve Falloff;
    }

    /// <summary>
    /// Handles camera movement and look interpolation.
    /// Has references and values relating to camera look and feel like shaking, fov and different view modes.
    /// Don't call the interface directly, use CameraController.I to control the camera state instead.
    /// </summary>
    public class CameraView : MonoBehaviour
    {
        #region variables
        public Camera Camera;
        public Transform LookTarget;
        public Transform FollowTarget;
        public bool SnapToPosition = false;
        public bool SnapToRotation = false;
        public float MovementSpeed = 5f;
        public float LookRotationSpeed = 4f;
        public AnimationCurve ShakeDamper;
        public AnimationCurve PitchFOV;
        public bool PitchFOVActive = false;

        private float startFOV;
        private bool shaking = false;
        private Quaternion currentLookRotation;
        private CoroutineManager ShakeCM;
        private Vector3 lookRotationUpVector = Vector3.up;

        public float FovMulti { get; private set; }

        #endregion
        #region init
        private void Awake()
        {
            ShakeCM = new CoroutineManager(this);

            startFOV = Camera.fieldOfView;
            FovMulti = 1;
        }
        #endregion
        #region logic
        public void FixedUpdate()
        {
            if (!shaking)
            {
                if (FollowTarget)
                {
                    if (SnapToPosition)
                    {
                        transform.position = FollowTarget.position;
                    }
                    else
                    {
                        float moveSpeed = MovementSpeed * Time.deltaTime;

                        transform.position = Vector3.Lerp(transform.position, FollowTarget.position, moveSpeed);
                    }
                }

                //rotate to look at target
                if (LookTarget)
                {
                    Vector3 lookDirection = Camera.transform.position.To(LookTarget.position);
                    Quaternion lookRotation = Quaternion.LookRotation(lookDirection, LookTarget.up);
                    if (SnapToRotation) currentLookRotation = lookRotation;
                    else currentLookRotation = Quaternion.Lerp(currentLookRotation, lookRotation, Time.deltaTime * LookRotationSpeed);
                }
            }

            if (LookTarget)
            {
                transform.rotation = currentLookRotation;// * currentAnalogInputLookRotation;
            }

            if (PitchFOVActive)
            {
                //change fov based on pitch over the horizon
                float pitchAngle = Helpers.AngleSigned(transform.forward, Vector3.ProjectOnPlane(transform.forward, lookRotationUpVector), transform.right);
                float percent = Mathf.Clamp01(pitchAngle / 45f);
                float pitchFov = 1 + PitchFOV.Evaluate(percent);

                Camera.fieldOfView = startFOV * FovMulti * pitchFov;
            }
        }
        #endregion
        #region public interface


        public void Shake(float duration, float force)
        {
            ShakeCM.Start(ShakeCoroutine(duration, force));
        }

        public void Shake(Transform origin, float maxDistance, float duration, float force, AnimationCurve falloffCurve)
        {
            float distance = Vector3.Distance(origin.position, Camera.transform.position);
            if (distance > maxDistance) return;
            float percent = (distance / maxDistance);
            float falloff = falloffCurve.Evaluate(percent);
            Shake(duration, force * falloff);
        }

        public void ShakeCamera(Transform origin, CameraShakeStats stats)
        {
            Shake(origin, stats.MaxDistance, stats.Duration, stats.Force, stats.Falloff);
        }

        public void InterruptShake()
        {
            ShakeCM.Stop();
            shaking = false;
        }

        public Transform GetLookTarget()
        {
            return LookTarget;
        }

        #endregion
        #region private interface
        private IEnumerator ShakeCoroutine(float duration, float force)
        {
            shaking = true;
            float totalTime = Time.time + duration;
            Vector3 originPosition = transform.position;

            while (totalTime > Time.time)
            {
                float percent = 1 - (totalTime - Time.time) / duration;
                float damper = ShakeDamper.Evaluate(percent);

                Vector3 forwardDirection = FollowTarget ? (FollowTarget.position - transform.position) : transform.forward;
                if (LookTarget)
                {
                    Vector3 lookDirection = Camera.transform.position.To(LookTarget.transform.position);
                    Quaternion lookRotation = Quaternion.LookRotation(lookDirection, Vector3.up);
                    currentLookRotation = Quaternion.Lerp(currentLookRotation, lookRotation, Time.deltaTime * LookRotationSpeed);

                    transform.rotation = currentLookRotation;
                    forwardDirection = lookDirection;
                }

                if (FollowTarget) originPosition = Vector3.Lerp(originPosition, FollowTarget.position, Time.deltaTime * MovementSpeed);
                transform.position = originPosition + Vector3.ProjectOnPlane(Random.insideUnitSphere * damper * force, forwardDirection);

                yield return new WaitForFixedUpdate();
            }

            shaking = false;
        }

        public void SetFovMulti(float multi)
        {
            FovMulti = multi;
        }

        private void LookAtInstant(Vector3 target)
        {
            transform.LookAt(target);
            currentLookRotation = transform.rotation;
        }
        #endregion
    }
}