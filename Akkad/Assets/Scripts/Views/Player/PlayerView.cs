﻿using UnityEngine;
using System;

[RequireComponent(typeof(PlayerMovementSystem))]
public class PlayerView : MonoBehaviour
{
    #region vars
    public PlayerMovementSystem MovementSystem;
    public PlayerInteractionSystem InteractionSystem;

    public int Health = 100;
    public float OnGroundRaycastDistance = 1.2f;
    public float FallDamageVelocity = 10f;
    public bool UseFallDamage = true;
    public Vector3 CenterPointOffset = Vector3.up;
    public Rigidbody Rigidbody;
    public CapsuleCollider Collider;
    public BoxCollider DeathCollider;
    public Transform CameraFollowTarget, CameraLookTarget, CameraLookTargetOffset;
    public Transform Graphics;
    public Vector2 CameraVerticalLookBounds = new Vector2(-3, 5);
    public float CameraVerticalLookSpeed = 10f;

    public bool JumpInputDown { get; private set; }
    public bool OnGround { get; private set; }
    public bool Dead { get; internal set; }

    public event Delegates.Action OnDeathEvent;
    public event Delegates.IntEvent OnChangeHealthEvent;
    public event Delegates.BoolEvent OnGroundEvent;
    public AI.SensorTarget SensorTarget;

    private Quaternion cameraLookTargetOffsetStartRotation;
    private float cameraLookTargetDefaultYOffset;
    private Vector3 cameraLookTargetLocalPositionOffset;

    #endregion
    #region init
    private void Awake()
    {
        JumpInputDown = false;
        MovementSystem = GetComponent<PlayerMovementSystem>();
        DeathCollider.enabled = false;

        //camera look setup
        cameraLookTargetLocalPositionOffset = CameraLookTarget.localPosition;
        cameraLookTargetDefaultYOffset = cameraLookTargetLocalPositionOffset.y;
        cameraLookTargetLocalPositionOffset.y = 0;
        cameraLookTargetOffsetStartRotation = CameraLookTargetOffset.localRotation;
    }

    private void Start()
    {
        SetHealth(Health);
        SetOnGround(true);

        InputController.I.OnSprintInput += OnSprintInput;
        EnableInput();
    }

    private void OnDestroy()
    {
        DisableInput();
    }

    public void EnableInput()
    {
        InputController.I.OnJumpInput += OnJumpInput;
        InputController.I.OnJumpUpInput += OnJumpUpInput;
        InputController.I.OnMouseAxisInput += OnMouseAxisInput;
        InputController.I.OnAxisInput += OnAxisInput;
        InputController.I.OnInteractInput += OnInteractInput;
        InputController.I.OnCameraLookAroundInput += OnCameraLookAroundPressed;
        InputController.I.OnCameraLookAroundUpInput += OnCameraLookAroundUp;
    }

    public void EnablePushInput()
    {
        InputController.I.OnMouseAxisInput += OnMouseAxisInput;
        InputController.I.OnAxisInput += OnAxisInput;
        InputController.I.OnInteractInput += OnInteractInput;
    }

    public void EnableLedgeGrabInput()
    {
        InputController.I.OnJumpInput += MovementSystem.LedgeGrabClimbUp;
        InputController.I.OnInteractInput += MovementSystem.LedgeGrabDropDown;
        InputController.I.OnAxisInput += MovementSystem.LedgeGrabAxisInput;
    }
    public void DisableLedgeGrabInput()
    {
        InputController.I.OnJumpInput -= MovementSystem.LedgeGrabClimbUp;
        InputController.I.OnInteractInput -= MovementSystem.LedgeGrabDropDown;
        InputController.I.OnAxisInput -= MovementSystem.LedgeGrabAxisInput;
    }

    public void EnableSprintInput()
    {
        InputController.I.OnSprintInput += OnSprintInput;
    }

    public void DisableInput()
    {
        InputController.I.OnJumpInput -= OnJumpInput;
        InputController.I.OnJumpUpInput -= OnJumpUpInput;
        InputController.I.OnMouseAxisInput -= OnMouseAxisInput;
        InputController.I.OnAxisInput -= OnAxisInput;
        InputController.I.OnInteractInput -= OnInteractInput;
        InputController.I.OnCameraLookAroundInput -= OnCameraLookAroundPressed;
        InputController.I.OnCameraLookAroundUpInput -= OnCameraLookAroundUp;

        JumpInputDown = false;
    }

    public void DisableSprintInput()
    {
        InputController.I.OnSprintInput -= OnSprintInput;
        MovementSystem.SetMovementSpeed(PlayerMovementSystem.MovementSpeedID.Walking);
    }

    public void ActivateMainCollider(bool active)
    {
        Rigidbody.isKinematic = !active;
        Collider.enabled = active;
    }

    #endregion
    #region logic
    private void Update()
    {
        //check interaction range
        InteractionSystem.UpdateInteractionRange();
        
        //check ledge grab
        if (!InteractionSystem.PushingObject)
            MovementSystem.UpdateLedgeGrab();

        if (MovementSystem.LedgeGrabbing) return;

        //ground raycast
        RaycastHit hitInfo;
        var rayPosition = transform.position + (transform.TransformDirection(CenterPointOffset));
        if (Physics.SphereCast(rayPosition, Collider.radius * 0.9f, -transform.up, out hitInfo, OnGroundRaycastDistance, LayerMasks.EntityGround))
        {
            if (!OnGround)
            {
                bool fallingDown = FallingDown();

                //check for upward slope movement
                if (Physics.Raycast(rayPosition + Rigidbody.velocity * Time.deltaTime, -transform.up, out hitInfo, OnGroundRaycastDistance, LayerMasks.EntityGround))
                {
                    fallingDown = true;
                }

                if (fallingDown)
                {
                    if (UseFallDamage && Math.Abs(Rigidbody.velocity.y) > FallDamageVelocity)
                    {
                        SetHealth(0);
                    }

                    SetOnGround(true);
                }
            }
        }
        else
            SetOnGround(false);

#if UNITY_EDITOR
        Debug.DrawRay(rayPosition, -transform.up * OnGroundRaycastDistance, Color.magenta);

        if (Input.GetKeyDown(KeyCode.K))
        {
            SetHealth(0);
        }
#endif
    }

    #endregion
    #region public interface

    public void SetOnGround(bool onGround)
    {
        if (onGround != OnGround)
        {
            if (OnGroundEvent != null) OnGroundEvent(onGround);
            MovementSystem.SetJumpDelay();
        }

        OnGround = onGround;
    }

    public void SetHealth(int health)
    {
        Health = health;

        if (Health <= 0)
        {
            Die();
        }

        if (OnChangeHealthEvent != null) OnChangeHealthEvent(Health);
    }

    #endregion
    #region private interface

    private bool FallingDown()
    {
        return Vector3.Dot(Rigidbody.velocity, transform.up) <= -0.1;
    }

    private void Die()
    {
        Rigidbody.velocity = Vector3.zero;
        Dead = true;
        Health = 0;
        DisableInput();
        enabled = false;
        MovementSystem.SetMoving(false);
        DeathCollider.enabled = true;
        Collider.enabled = false;
        Rigidbody.constraints = RigidbodyConstraints.None;
        SensorTarget.VisibleByDefault = false;

        if (OnDeathEvent != null) OnDeathEvent();
    }

    #endregion
    #region events
    private void OnMouseAxisInput(float xAxis, float yAxis)
    {
        if (MovementSystem.DisableRotationHack) return;

        if (cameraLookingAround)
        {
            //only look around, no character rotation
            CameraLookTargetOffset.localRotation = Quaternion.AngleAxis(xAxis * 300f * Time.deltaTime, Vector3.up) * CameraLookTargetOffset.localRotation;

            CameraLookTargetOffset.localRotation = Quaternion.AngleAxis(-yAxis * 300f * Time.deltaTime, Vector3.right) * CameraLookTargetOffset.localRotation;

            Vector3 euler = CameraLookTargetOffset.localRotation.eulerAngles;
            float x = CameraLookTargetOffset.localRotation.eulerAngles.x;

            //clamping rotation
            if (x > 180 && x < 310)
                euler.x = 310;
            else if (x > 45 && x <= 180)
                euler.x = 45;

            euler.z = 0;
            CameraLookTargetOffset.localRotation = Quaternion.Euler(euler);
        }
        else
        {
            //look up and down
            cameraLookTargetDefaultYOffset += yAxis * CameraVerticalLookSpeed * Time.deltaTime;

            cameraLookTargetDefaultYOffset = Mathf.Clamp(cameraLookTargetDefaultYOffset, CameraVerticalLookBounds.x, CameraVerticalLookBounds.y);

            CameraLookTarget.localPosition = cameraLookTargetLocalPositionOffset + Vector3.up * cameraLookTargetDefaultYOffset;

            CameraLookTargetOffset.localRotation = Quaternion.Lerp(CameraLookTargetOffset.localRotation, cameraLookTargetOffsetStartRotation, Time.deltaTime * 2f);

            //rotate character
            MovementSystem.TurnOnAxis(xAxis);
        }
    }
    
    private void OnInteractInput()
    {
        InteractionSystem.InteractWithinRange();
    }

    private void OnAxisInput(float xAxis, float zAxis)
    {
        MovementSystem.MoveOnAxis(xAxis, zAxis);
    }

    private void OnSprintInput(bool sprint)
    {
        MovementSystem.SetMovementSpeed(sprint ? PlayerMovementSystem.MovementSpeedID.Running : PlayerMovementSystem.MovementSpeedID.Walking);
    }

    private void OnJumpInput()
    {
        if (!JumpInputDown && OnGround && MovementSystem.CanJump)
        {
            JumpInputDown = true;
            MovementSystem.Jump();
        }
    }

    private void OnJumpUpInput()
    {
        JumpInputDown = false;
    }

    bool cameraLookingAround = false;

    private void OnCameraLookAroundPressed()
    {
        cameraLookingAround = true;
    }

    private void OnCameraLookAroundUp()
    {
        cameraLookingAround = false;
    }

    #endregion
}
