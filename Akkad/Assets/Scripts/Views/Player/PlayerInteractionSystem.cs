﻿using UnityEngine;
using InteractionSystem;
using System.Collections;
using System;

public class PlayerInteractionSystem : MonoBehaviour
{
    #region vars
    public float InteractionRadius = 1.4f;
    public Transform InteractionSphereCenter, PushableObjectRaycastPosition;
    public event Delegates.StringEvent OnInteractionRangeEnterEvent, OnInteractionRangeExitEvent;
    public event Delegates.Action OnPushStartEvent, OnPushEndEvent;
    public bool PushingObject { get; private set; }
    public float PushStartLerpDuration = 0.5f;

    private CoroutineManager PushCM;
    private PlayerView player;
    private bool interactionObjectsInRange = false;
    private PushableObject pushableObject;
    #endregion
    #region init
    private void Start()
    {
        player = GetComponent<PlayerView>();
        PushCM = new CoroutineManager(this);
        PushingObject = false;
    }
    #endregion
    #region logic

    public void UpdateInteractionRange()
    {
        var hitColliders = Physics.OverlapSphere(InteractionSphereCenter.position, InteractionRadius, LayerMasks.Interactable);
        if (hitColliders.Length > 0)
        {
            if (interactionObjectsInRange == false)
            {
                interactionObjectsInRange = true;
                var interactionLayer = hitColliders[0].GetComponent<InteractionLayer>();

                if (interactionLayer && OnInteractionRangeEnterEvent != null)
                    OnInteractionRangeEnterEvent(interactionLayer.InteractionMessage);
            }
        }
        else
        {
            interactionObjectsInRange = false;
            if (OnInteractionRangeExitEvent != null)
                OnInteractionRangeExitEvent("");
        }
    }

    public void InteractWithinRange()
    {
        //check for pushable objects first
        if (PushingObject)
        {
            //stop pushing
            PushCM.Stop();
            PushCM.Start(PushEndCoroutine());
        }
        else if (!PushCM.Running)
        {
            RaycastHit hitInfo;
            Vector3 downVector = -transform.up;

            float raycastLength = 0.3f;
            Vector3 ledgeGrabRayOrigin = PushableObjectRaycastPosition.position + Vector3.up * raycastLength;
            if (!PushingObject && Physics.Raycast(ledgeGrabRayOrigin, downVector, out hitInfo, raycastLength, LayerMasks.Interactable))
            {
                var pushObject = hitInfo.collider.transform;

                PushCM.Start(PushStartCoroutine(pushObject));
            }
        }

        //check for interactable objects
        var hitColliders = Physics.OverlapSphere(InteractionSphereCenter.position, InteractionRadius, LayerMasks.Interactable);

        for (int i = 0; i < hitColliders.Length; i++)
        {
            //send activation input to all interaction triggers in range
            var interactionLayer = hitColliders[i].GetComponent<InteractionLayer>();
            if (interactionLayer)
            {
                interactionLayer.ReceiveInput(new Data(TypeID.Activate));
            }
        }
    }
    #endregion
    #region public interface
    #endregion
    #region private interface
    private IEnumerator PushStartCoroutine(Transform interactionObject)
    {
        PushingObject = true;
        if (OnPushStartEvent != null) OnPushStartEvent();
        pushableObject = interactionObject.GetComponentInParent<PushableObject>();

        //disable control and physics
        player.MovementSystem.SetMoving(false);
        player.MovementSystem.SetMovementSpeed(PlayerMovementSystem.MovementSpeedID.Walking);
        player.DisableInput();
        player.DisableSprintInput();
        player.ActivateMainCollider(false);

        var startRotation = transform.rotation;
        //calculate end rotation
        var playerForward = -Vector3.forward;
        var objectToPlayerLocalDirection = interactionObject.worldToLocalMatrix * interactionObject.position.To(transform.position);
        float angle = Helpers.AngleSigned(Vector3.forward, objectToPlayerLocalDirection, Vector3.up);
        if (angle > 135 || angle < -135)
        {
            playerForward = Vector3.forward;
        }
        else if (angle > 45)
        {
            playerForward = -Vector3.right;
        }
        else if (angle < -45)
        {
            playerForward = Vector3.right;
        }

        Debug.DrawRay(interactionObject.position, interactionObject.localToWorldMatrix * playerForward, Color.magenta, 2f);

        var collider = interactionObject.GetComponent<Collider>();
        var colliderForward = -playerForward;
        colliderForward.x *= collider.bounds.extents.x;
        colliderForward.y *= collider.bounds.extents.y;
        colliderForward.z *= collider.bounds.extents.z;

        var targetRotation = Quaternion.LookRotation(interactionObject.localToWorldMatrix * playerForward, Vector3.up);

        //calculate positions
        var startPosition = transform.position;
        
        Vector3 offset = interactionObject.localToWorldMatrix * colliderForward;
        offset.y = 0;
        var playerOffset = (Vector3)(transform.localToWorldMatrix * PushableObjectRaycastPosition.localPosition);
        playerOffset.y = 0;
        offset -= playerOffset;

        var targetPosition = pushableObject.transform.position + offset - Vector3.up * collider.bounds.extents.y;
        Debug.DrawRay(targetPosition, Vector3.up, Color.blue, 2f);

        //lerp position and rotation
        float percent = 0f;
        while (percent < 1)
        {
            percent += Time.deltaTime / PushStartLerpDuration;
            transform.position = Vector3.Lerp(startPosition, targetPosition, percent);
            transform.rotation = Quaternion.Lerp(startRotation, targetRotation, percent);
            yield return null;
        }
        transform.rotation = targetRotation;
        transform.position = targetPosition;

        //reenable control and physics
        player.EnablePushInput();
        player.ActivateMainCollider(true);
        player.SetOnGround(true);

        PushCM.Start(PushingCoroutine(offset, pushableObject));
    }

    private IEnumerator PushingCoroutine(Vector3 offset, PushableObject pushableObject)
    {
        var collider = pushableObject.GetComponentInChildren<Collider>();
        var yOffset = collider.bounds.extents.y * Vector3.up;

        pushableObject.StartPush();
        var rigidbody = pushableObject.Rigidbody;
        var localDirection = transform.worldToLocalMatrix * pushableObject.transform.forward;
        var rotationDifference = Quaternion.LookRotation(localDirection, Vector3.up);

        while (true)
        {
            var targetPosition = transform.position + transform.rotation * (Vector3.forward * ( offset.magnitude)) + yOffset;

            rigidbody.MoveRotation(Quaternion.Lerp(rigidbody.rotation, transform.rotation * rotationDifference, Time.deltaTime * 20f));
            rigidbody.MovePosition(Vector3.Lerp(rigidbody.position, targetPosition, Time.deltaTime * 20f));

            yield return null;
        }
    }

    private IEnumerator PushEndCoroutine()
    {
        if (OnPushEndEvent != null) OnPushEndEvent();

        //disable control and physics
        player.MovementSystem.SetMoving(false);
        player.DisableInput();
        player.ActivateMainCollider(false);

        yield return new WaitForSeconds(1.5f);

        //reenable control and physics
        player.EnableInput();
        player.EnableSprintInput();
        player.ActivateMainCollider(true);
        player.SetOnGround(true);

        PushingObject = false;
        pushableObject.StopPush();
        pushableObject = null;
    }
    #endregion
    #region events

    private void OnDrawGizmos()
    {
        //debug interactionSphere
        Gizmos.color = new Color(1, 1, 1, 0.5f);
        Gizmos.DrawSphere(InteractionSphereCenter.position, InteractionRadius);
    }

    #endregion
}
