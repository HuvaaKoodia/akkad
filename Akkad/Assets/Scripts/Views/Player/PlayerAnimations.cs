﻿using UnityEngine;
using System.Collections;
using System;

/// <summary>
/// The interface between the player and the player animator.
/// Knows about the player, but the player doesn't know (or care) about the animations
/// 
/// TODO:
/// - Optimize Animator variable ids to ints
/// </summary>
public class PlayerAnimations : MonoBehaviour 
{
    #region vars
    public Animator Animator;
    public float SpeedLerpDuration = 0.5f;
    public float MovementDirectionLerpSpeed = 2f;
    public float TurningLerpSpeed = 4;

    private PlayerView player;
    private float turningAmount = 0;
    private Vector3 movementDirectionVector = Vector3.zero;
    private CoroutineManager speedLerpCM;
    #endregion
    #region init
    private void Awake () 
	{
        speedLerpCM = new CoroutineManager(this);

        player = GetComponent<PlayerView>();
        player.OnGroundEvent += OnGround;
        player.OnChangeHealthEvent += OnHealthChange;
        player.InteractionSystem.OnPushStartEvent += OnPushStart;
        player.InteractionSystem.OnPushEndEvent += OnPushEnd;
        player.MovementSystem.OnMovementStartEvent += OnMovement;
        player.MovementSystem.OnMovementSpeedChanged += OnMovementSpeedChanged;
        player.MovementSystem.OnJumpEvent += OnJump;
        player.MovementSystem.OnTurningEvent += OnTurning;
        player.MovementSystem.OnLedgeGrabStartEvent += OnLedgeGrabStart;
        player.MovementSystem.OnLedgeGrabClimbUpEvent += OnLedgeGrabClimbUp;
        player.MovementSystem.OnLedgeGrabDropDownEvent += OnLedgeGrabDropDown;
        player.MovementSystem.OnChangeDirectionEvent += OnChangeDirection;
        player.MovementSystem.OnLedgeMovementEvent += OnLedgeMovement;
        player.MovementSystem.OnMovementEvent += OnMovement;
    }

    private void OnEnable()
    {
        var listener = Animator.GetListener("LedgeGrabEnd");
        listener.OnEnterEvent.AddListener(OnLedgeClimbEndAnimationEvent);
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    /// <summary>
    /// Lerps movement speed to target speed
    /// </summary>
    private IEnumerator SpeedLerpCoroutine(float targetSpeed, float lerpDuration)
    {
        float startSpeed = Animator.GetFloat("Speed");
        float percent = 0;
        while (true)
        {
            percent += Time.deltaTime / lerpDuration;
            float speed = Mathf.Lerp(startSpeed, targetSpeed, percent);
            Animator.SetFloat("Speed", speed);
            if (speed == targetSpeed) break;
            yield return null;
        }
    }
    #endregion
    #region events

    private void OnMovement(Vector3 velocity)
    {
        //movement direction. quick hack
        var localVelocity = player.transform.worldToLocalMatrix * player.Rigidbody.velocity;
        localVelocity.y = 0;
        localVelocity.Normalize();

        float multiplier = Mathf.Min(1, Vector3.Distance(movementDirectionVector, localVelocity));

        movementDirectionVector = Vector3.Lerp(movementDirectionVector, localVelocity, MovementDirectionLerpSpeed * multiplier * Time.deltaTime);

        Animator.SetFloat("MovementDirectionX", movementDirectionVector.x);
        Animator.SetFloat("MovementDirectionY", movementDirectionVector.z);
    }

    private void OnLedgeMovement(float horizontalMovement)
    {
        Animator.SetFloat("MovementDirectionX", horizontalMovement);
    }

    private void OnLedgeGrabStart()
    {
        Animator.SetTrigger("LedgeGrab");
    }

    private void OnLedgeGrabClimbUp()
    {
        Animator.SetTrigger("LedgeGrabClimbUp");
    }

    private void OnLedgeGrabDropDown()
    {
        Animator.SetTrigger("LedgeGrabDropDown");
    }

    private void OnJump()
    {
        Animator.SetTrigger("Jump");
    }

    private void OnMovement(bool moving)
    {
        Animator.SetBool("Moving", moving);

        Animator.SetFloat("MovementDirectionX", 0);
        Animator.SetFloat("MovementDirectionY", 0);
        Animator.SetFloat("TurningAmount", 0);
    }

    private void OnGround(bool onGround)
    {
        Animator.SetBool("OnGround", onGround);
    }

    private void OnMovementSpeedChanged(float speed)
    {
        speedLerpCM.Start(SpeedLerpCoroutine((speed - player.MovementSystem.WalkingSpeed) / (player.MovementSystem.RunningSpeed - player.MovementSystem.WalkingSpeed), SpeedLerpDuration));
    }

    private void OnTurning(float value)
    {
        float target = 0;
        if (value < 0) target = -1;
        else if (value > 0) target = 1;
        turningAmount = Mathf.Lerp(turningAmount, target, Time.deltaTime * TurningLerpSpeed);
        Animator.SetFloat("TurningAmount", turningAmount);
    }

    private void OnLedgeClimbEndAnimationEvent()
    {
        player.MovementSystem.SetLedgeGrabEndTrigger();
    }

    private void OnHealthChange(int health)
    {
        if (health == 0)
        {
            Animator.SetTrigger("Dead");
        }
    }

    private void OnPushStart()
    {
        Animator.SetBool("Pushing", true);
    }

    private void OnPushEnd()
    {
        Animator.SetBool("Pushing", false);
    }

    private void OnChangeDirection()
    {
        Animator.SetTrigger("ChangeDirection");
    }

    #endregion
}
