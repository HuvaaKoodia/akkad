﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayerMovementSystem : MonoBehaviour
{
    #region vars

    public enum MovementSpeedID
    {
        Walking,
        Running
    }

    public float WalkingSpeed = 2;
    public float WalkingBackwardsSpeedMultiplier = 0.5f;
    public float RunningSpeed = 4.5f;
    public float MovementSpeedLerpSpeed = 1f;
    public float MouseRotationSpeed = 150f;
    public float RunRotationSpeed = 2f;
    public float JumpStartDelay = 0.35f;
    public float JumpRepeatDelay = 0.6f;
    public float JumpImpulse = 4;
    public float JumpForce = 3;
    public float JumpForceReductionMultiplier = 0.1f;
    public float LedgeGrabInputStartDelay = 0.3f;
    public float LedgeGrabStartLerpDuration = 0.5f;
    public float LedgeGrabEndLerpDuration = 0.5f;
    public float LedgeGrabChangeLedgeLerpDuration = 0.15f;
    public float LedgeMovementSpeed = 0.1f;
    public float LedgeMovementLerpSpeed = 3f;
    public bool UseRunningRotation = false;
    public Transform LedgeGrabPosition, LedgeGrabRaycastPosition;
    public PhysicMaterial ZeroFriction;
    public PhysicMaterial RegularFriction;
    public GravityWellTarget GravityWellTarget;

    public bool Moving { get; private set; }
    public bool LedgeGrabbing { get; private set; }
    public bool CanJump { get { return !jumpCM.Running && Time.time > jumpRepeatDelay; } }
    public bool DisableRotationHack = false;

    public event Delegates.Action OnLedgeGrabStartEvent, OnLedgeGrabClimbUpEvent, OnLedgeGrabDropDownEvent;
    public event Delegates.Action OnJumpEvent;
    public event Delegates.Action OnChangeDirectionEvent;
    public event Delegates.BoolEvent OnMovementStartEvent;
    public event Delegates.FloatEvent OnMovementSpeedChanged;
    public event Delegates.FloatEvent OnTurningEvent;
    public event Delegates.FloatEvent OnLedgeMovementEvent;
    public event Delegates.Vector3Event OnMovementEvent;

    private Rigidbody Rigidbody;
    private PlayerView player;
    private float additionalJumpForce = 0;
    private float jumpRepeatDelay = 0, jumpStartDelay = 0;
    private CoroutineManager jumpCM;
    private bool ledgeGrabEndTrigger = false;
    private float movementSpeed, movementSpeedTarget;
    private MovementSpeedID movementSpeedID;
    private bool wallInFront = false;
    private float oldVelocityForwardMagnitude = 0;
    private bool onlyForwardMovement = false;
    private Vector3 gravityDownNormal, groundUpNormal, groundUpNormalRaw;
    private bool droppedDown = false;
    private LedgeObject currentLedgeObject;
    private float currentLedgePositionT;
    private float rotationSpeedHack = 10;//unused

    private Quaternion startGraphicsRotation;
    private Rigidbody groundRigidbody;
    #endregion
    #region init

    private void Start()
    {
        player = GetComponent<PlayerView>();
        Rigidbody = player.Rigidbody;
        jumpCM = new CoroutineManager(this);
        LedgeGrabbing = false;
        SetMovementSpeed(MovementSpeedID.Walking);
        GravityWellTarget.OnGravityAppliedEvent += OnGravityWellApplied;
        player.OnGroundEvent += OnGround;

        //normal normals
        gravityDownNormal = Vector3.down;
        groundUpNormal = Vector3.up;
        groundUpNormalRaw = Vector3.up;

        startGraphicsRotation = player.Graphics.rotation;
    }

    #endregion
    #region logic
    private void Update()
    {
        if (Physics.gravity == Vector3.zero)
        {
            //down raycast for ground normal
            Vector3 currentGroundNormal = groundUpNormal;
            RaycastHit hit;
            var rayPosition = transform.position + transform.up * 0.1f;
            if (Physics.Raycast(rayPosition, gravityDownNormal, out hit, 100, LayerMasks.Ground))
            {
                MeshCollider collider = (MeshCollider)hit.collider;
                if (collider)
                {
                    Mesh mesh = collider.sharedMesh;
                    Vector3[] normals = mesh.normals;
                    int[] triangles = mesh.triangles;

                    Vector3 n0 = normals[triangles[hit.triangleIndex * 3 + 0]];
                    Vector3 n1 = normals[triangles[hit.triangleIndex * 3 + 1]];
                    Vector3 n2 = normals[triangles[hit.triangleIndex * 3 + 2]];
                    Vector3 baryCenter = hit.barycentricCoordinate;
                    Vector3 interpolatedNormal = n0 * baryCenter.x + n1 * baryCenter.y + n2 * baryCenter.z;
                    interpolatedNormal.Normalize();
                    interpolatedNormal = hit.transform.TransformDirection(interpolatedNormal);

                    currentGroundNormal = interpolatedNormal;
                }

                groundRigidbody = collider.GetComponent<Rigidbody>();
            }

            /*
            if (player.OnGround)
            {
                rotationSpeedHack = 10;
            }
            else
            {
                //rotationSpeedHack = Mathf.Lerp(rotationSpeedHack, 1, Time.deltaTime);
            }*/

            groundUpNormalRaw = currentGroundNormal;
            groundUpNormal = Vector3.Lerp(groundUpNormal, currentGroundNormal, Time.deltaTime * rotationSpeedHack);
            groundUpNormal = groundUpNormalRaw;

            if (currentGroundNormal != groundUpNormalRaw)
            {
                //adjust velocity when changing normals
                Rigidbody.velocity = Quaternion.FromToRotation(groundUpNormal, currentGroundNormal) * Rigidbody.velocity;
            }

            Debug.DrawLine(rayPosition, gravityDownNormal * 100, Color.magenta);

            //rotate towards ground normal
            Vector3 forwardDirection = Vector3.ProjectOnPlane(transform.forward, groundUpNormal).normalized;
            transform.rotation = Quaternion.LookRotation(forwardDirection, groundUpNormal);

            //rotate graphics separately for smoother visible rotation
            startGraphicsRotation = Quaternion.Lerp(startGraphicsRotation, transform.rotation, Time.deltaTime * 3);
            player.Graphics.rotation = startGraphicsRotation;
        }

        //forward raycast for walls (stops moving automatically)
        wallInFront = Physics.Raycast(transform.position + player.CenterPointOffset, transform.forward, 0.5f, LayerMasks.Wall | LayerMasks.Prop);
    }

    private void FixedUpdate()
    {
        if (Physics.gravity != Vector3.zero)
        {
            ApplyGravity(Physics.gravity);
        }
    }

    private void ApplyGravity(Vector3 gravity)
    {
        var magnitude = Vector3.Dot(Rigidbody.velocity, gravityDownNormal);
        if (magnitude < 0) magnitude = 0;

        gravityDownNormal = gravity.normalized;

        Rigidbody.velocity += gravity * Time.fixedDeltaTime;
    }

    #endregion
    #region public interface

    public void Jump()
    {
        jumpCM.Start(JumpCoroutine());
    }

    public void SetLedgeGrabEndTrigger()
    {
        ledgeGrabEndTrigger = true;
    }

    public void MoveOnAxis(float xAxis, float zAxis)
    {
        //calculate direction and speed
        bool running = movementSpeedTarget == RunningSpeed;
        float speedTarget = movementSpeedTarget;

        if (onlyForwardMovement) zAxis = Mathf.Abs(zAxis);
        Vector3 direction = transform.forward * zAxis + transform.right * xAxis;

        float forwardDirectionMagnitude = Vector3.Dot(direction, transform.forward);
        float velocityForwardMagnitude = Vector3.Dot(Rigidbody.velocity, transform.forward);

        //restrict forward movement if stopped by a wall
        if (player.OnGround && wallInFront && forwardDirectionMagnitude > 0)
            direction = Vector3.ProjectOnPlane(direction, transform.forward);

        //changing direction hacks! Unused
        if (player.OnGround && running && forwardDirectionMagnitude < 0)
        {
            if (oldVelocityForwardMagnitude < 0)
            {
                //only moving backwards -> use walking speed
                running = false;
                speedTarget = WalkingSpeed;
            }
        }

        //apply backwards moving speed multiplier
        if (!running && forwardDirectionMagnitude < -0.02) speedTarget *= WalkingBackwardsSpeedMultiplier;

        movementSpeed = Mathf.Lerp(movementSpeed, speedTarget, MovementSpeedLerpSpeed * Time.deltaTime);

        //apply direction and speed. Preserves gravity
        float downForce = Vector3.Dot(Rigidbody.velocity, -groundUpNormal);
        Rigidbody.velocity = direction * movementSpeed + -groundUpNormal * downForce;

        //Rigidbody.velocity += direction * speed * Time.deltaTime;

        oldVelocityForwardMagnitude = velocityForwardMagnitude;

        //running rotation
        if (UseRunningRotation && running && player.OnGround && Rigidbody.velocity.magnitude > 0.2f)
        {
            float runRotation = Helpers.AngleSigned(transform.forward, Rigidbody.velocity.normalized, groundUpNormal);
            if (Mathf.Abs(runRotation) > 90) runRotation = Mathf.Abs(runRotation);//glitch fix
            var targetRotation = transform.rotation * Quaternion.AngleAxis(runRotation, Vector3.up);
            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, RunRotationSpeed * Time.deltaTime);
        }

        //set movement
        if (xAxis == 0 && zAxis == 0)
            SetMoving(false);
        else
            SetMoving(true);

        if (zAxis == 0)
            onlyForwardMovement = false;

        if (OnMovementEvent != null) OnMovementEvent(Rigidbody.velocity);
    }

    public void SetJumpDelay()
    {
        jumpRepeatDelay = Time.time + JumpRepeatDelay;
    }

    public void SetMoving(bool moving)
    {
        if (Moving == moving) return;

        if (moving)
        {
            if (OnMovementStartEvent != null && !Moving) OnMovementStartEvent(true);
            Moving = true;
            player.Collider.material = ZeroFriction;
        }
        else
        {
            if (OnMovementStartEvent != null && Moving) OnMovementStartEvent(false);
            Moving = false;
            player.Collider.material = RegularFriction;
            movementSpeed = 0;
        }
    }

    public void SetMovementSpeed(MovementSpeedID id)
    {
        //no acceleration in air
        if (!player.OnGround && id == MovementSpeedID.Running) return;

        float speed = id == MovementSpeedID.Walking ? WalkingSpeed : RunningSpeed; //lazy hack!
        if (OnMovementSpeedChanged != null && id != movementSpeedID) OnMovementSpeedChanged(speed);
        movementSpeedTarget = speed;
        movementSpeedID = id;
    }

    public void TurnOnAxis(float axis)
    {
        if (DisableRotationHack) return;

        transform.rotation = Quaternion.AngleAxis(axis * MouseRotationSpeed * Time.deltaTime, groundUpNormal) * transform.rotation;
        if (OnTurningEvent != null) OnTurningEvent(axis);
    }

    public void UpdateLedgeGrab()
    {
        if (!LedgeGrabbing && !droppedDown)
        {
            //start ledge grab raycast
            LedgeObject ledgeObject;

            if (GetLedgeObject(Vector3.zero, out ledgeObject))
            {
                StartCoroutine(LedgeGrabStartCoroutine(ledgeObject, LedgeGrabPosition.position, LedgeGrabStartLerpDuration));
            }
        }
    }

    public void LedgeGrabClimbUp()
    {
        if (currentLedgeObject.CanClimbUp)
            StartCoroutine(LedgeGrabEndCoroutine());
    }

    public void LedgeGrabDropDown()
    {
        droppedDown = true;
        if (OnLedgeGrabDropDownEvent != null) OnLedgeGrabDropDownEvent();

        //reenable control and physics
        player.DisableLedgeGrabInput();
        player.EnableInput();
        player.ActivateMainCollider(true);

        LedgeGrabbing = false;
    }

    public void LedgeGrabAxisInput(float xAxis, float zAxis)
    {
        var partNormal = currentLedgeObject.Spline.GetNormal(currentLedgePositionT, transform.up);
        float length = currentLedgeObject.Spline.GetPartLength(currentLedgePositionT, true);
        float speed = (LedgeMovementSpeed) / length;

        //rotate based on spline normal
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(-partNormal, Vector3.up), Time.deltaTime * LedgeMovementLerpSpeed);

        //check for ledges on the way -> change ledge
        RaycastHit hitInfo;
        var rayPosition = transform.position + player.CenterPointOffset;
        var rayDirection = transform.right * Math.Sign(xAxis);
        if (Physics.SphereCast(rayPosition, 0.1f, rayDirection, out hitInfo, 0.4f, LayerMasks.Ledge))
        {
            var ledge = hitInfo.collider.GetComponentInParent<LedgeObject>();

            if (ledge != currentLedgeObject)
            {
                //change ledge 
                player.DisableLedgeGrabInput();
                StartCoroutine(LedgeGrabStartCoroutine(ledge,rayPosition + rayDirection * 0.4f, LedgeGrabChangeLedgeLerpDuration));
                if (OnLedgeMovementEvent != null) OnLedgeMovementEvent(0);
                return;
            }
        } 

        Debug.DrawRay(transform.position + player.CenterPointOffset, transform.right * Math.Sign(xAxis) * (0.1f + speed), Color.blue);

        //check for walls on the way -> cannot move

        if (Physics.SphereCast(rayPosition, 0.1f, rayDirection, out hitInfo, 0.1f + speed, LayerMasks.Wall | LayerMasks.Prop))
        {
            if (OnLedgeMovementEvent != null) OnLedgeMovementEvent(0);
            return;
        }

        //move character
        currentLedgePositionT += Time.deltaTime * xAxis * speed;
        if (currentLedgePositionT < 0) currentLedgePositionT = 1 + currentLedgePositionT;
        if (currentLedgePositionT > 1) currentLedgePositionT = 1 - currentLedgePositionT;

        transform.position = currentLedgeObject.GetPosition(currentLedgePositionT) + LedgeGrabPosition.To(transform);

        if (OnLedgeMovementEvent != null) OnLedgeMovementEvent(xAxis);
    }

    #endregion
    #region private interface

    private IEnumerator LedgeGrabStartCoroutine(LedgeObject ledgeObject, Vector3 closePosition, float ledgeGrabStartLerpDuration)
    {
        if (!LedgeGrabbing)
            if (OnLedgeGrabStartEvent != null) OnLedgeGrabStartEvent();

        LedgeGrabbing = true;
        currentLedgeObject = ledgeObject;

        //disable control and physics
        SetMoving(false);
        player.DisableInput();
        player.ActivateMainCollider(false);

        //calculate rotations
        currentLedgePositionT = ledgeObject.GetClosestF(closePosition);
        var ledgeNormal = ledgeObject.Spline.GetNormal(currentLedgePositionT, transform.up);
        var startRotation = transform.rotation;
        var endRotation = Quaternion.LookRotation(-ledgeNormal, Vector3.up);

        //calculate positions
        var startPosition = transform.position;
        var endPosition = ledgeObject.GetPosition(currentLedgePositionT) - endRotation * LedgeGrabPosition.localPosition;

        //lerp position and rotation
        float percent = 0f;
        while (percent < 1)// && !ledgeGrabEndTrigger)
        {
            percent += Time.deltaTime / ledgeGrabStartLerpDuration;
            transform.position = Vector3.Lerp(startPosition, endPosition, percent);

            transform.rotation = Quaternion.Lerp(startRotation, endRotation, percent);
            yield return null;
        }

        transform.rotation = endRotation;
        transform.position = endPosition;

        yield return new WaitForSeconds(LedgeGrabInputStartDelay);

        player.EnableLedgeGrabInput();
    }

    private IEnumerator LedgeGrabEndCoroutine()
    {
        if (OnLedgeGrabClimbUpEvent != null) OnLedgeGrabClimbUpEvent();

        //disable control and physics
        SetMoving(false);

        player.DisableLedgeGrabInput();
        player.ActivateMainCollider(false);

        ledgeGrabEndTrigger = false;

        //let animation run
        while (!ledgeGrabEndTrigger) yield return null;

        //lerp to end position
        {
            float percent = 0f;
            var startPosition = transform.position;
            var endPosition = currentLedgeObject.GetPosition(currentLedgePositionT);
            while (percent < 1)
            {
                percent += Time.deltaTime / LedgeGrabEndLerpDuration;
                transform.position = Vector3.Lerp(startPosition, endPosition, percent);
                yield return null;
            }
            transform.position = endPosition;
        }

        //reenable control and physics
        player.EnableInput();
        player.ActivateMainCollider(true);
        player.SetOnGround(true);

        LedgeGrabbing = false;
    }

    private IEnumerator JumpCoroutine()
    {
        jumpStartDelay = Time.time + JumpStartDelay * (1 - (Rigidbody.velocity.magnitude / RunningSpeed));
        additionalJumpForce = JumpForce;
        if (OnJumpEvent != null) OnJumpEvent();//starts animation

        while (Time.time < jumpStartDelay) yield return null;

        if (player.OnGround)
        {
            //initial impulse happens even if the button was already released
            Rigidbody.AddForce(groundUpNormal * JumpImpulse, ForceMode.Impulse);
            player.SetOnGround(false);
        }
        else yield break;

        //wait one frame for Unity physics system to process the AddForce
        yield return null;

        //additional force happens only if button still pressed and character hasn't started falling down yet due to gravity
        while (player.JumpInputDown && additionalJumpForce > 0 && Vector3.Dot(Rigidbody.velocity, groundUpNormal) > 0)
        {
            Rigidbody.AddForce(groundUpNormal * additionalJumpForce, ForceMode.Force);
            additionalJumpForce -= Time.deltaTime * JumpForceReductionMultiplier;
            yield return null;
        }
    }

    private IEnumerator ChangeDirectionCoroutine()
    {
        if (OnChangeDirectionEvent != null) OnChangeDirectionEvent();
        player.DisableInput();

        yield return new WaitForSeconds(1f);

        player.transform.rotation = player.transform.rotation * Quaternion.AngleAxis(180, groundUpNormal);
        player.EnableInput();
    }

    private bool GetLedgeObject(Vector3 originOffset, out LedgeObject ledgeObject)
    {
        Vector3 downVector = -transform.up;

        float ledgeGrabRayDistance = 0.3f;
        Vector3 ledgeGrabRayOrigin = LedgeGrabRaycastPosition.position + originOffset;
        var ledgeObjects = Physics.RaycastAll(ledgeGrabRayOrigin, downVector, ledgeGrabRayDistance, LayerMasks.Ledge);

        for (int i = 0; i < ledgeObjects.Length; i++)
        {
            var hitInfo = ledgeObjects;
            ledgeObject = hitInfo[i].collider.GetComponentInParent<LedgeObject>();

            if (ledgeObject == null) return false;

            //calculate end rotation
            var objectToPlayerLocalDirection = ledgeObject.transform.worldToLocalMatrix * ledgeObject.transform.position.To(transform.position);
            float angle = Helpers.AngleSigned(Vector3.forward, objectToPlayerLocalDirection, Vector3.up);
            if (angle > 135 || angle < -135)
            {
                if (!ledgeObject.EnableBackGrab) continue;
            }
            else if (angle > 45)
            {
                if (!ledgeObject.EnableRightGrab) continue;
            }
            else if (angle < -45)
            {
                if (!ledgeObject.EnableLeftGrab) continue;
            }
            else
            {
                if (!ledgeObject.EnableForwardGrab) continue;
            }
            return true;
        }

        ledgeObject = null;

        return false;
    }

    #endregion
    #region events
    private void OnGravityWellApplied(Vector3 vector)
    {
        ApplyGravity(vector);
    }

    private void OnGround(bool value)
    {
        if (value) droppedDown = false;
    }
    #endregion
}
