﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using InteractionSystem;
using UnityEngine.UI;

public class ButtonView : MonoBehaviour {

    public UnityEvent OnPressedEvent;
    public InteractionLayer InteractionLayer;

    void Start() {
        InteractionLayer.OnInputEvent += OnInputEvent;

    }

    private void OnInputEvent(Data data)
    {
        if (data.Type == TypeID.Activate)
            OnPressedEvent.Invoke();
    }

}
