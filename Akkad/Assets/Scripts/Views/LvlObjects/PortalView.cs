﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cameras;

public class PortalView : MonoBehaviour
{
    #region vars
    public PortalView Target;
    public Camera Camera;
    public MeshRenderer Renderer;

    public Material RenderTextureMaterial { get; private set; }
    [SerializeField]
    private RenderTexture RenderTextureSource;
    [SerializeField]
    private Material RenderTextureMaterialSource;

    #endregion
    #region init
    private void Awake()
    {
        RenderTextureMaterial = new Material(RenderTextureMaterialSource);
        Camera.targetTexture = RenderTextureSource;
    }

    private void Start()
    {
        if (!Target)
        {
            Debug.LogErrorFormat("Portal {0} has no Target", gameObject.name);
            return;
        }

        Renderer.material = Target.RenderTextureMaterial;
        StartCoroutine(RotateTargetCamera());
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    private IEnumerator RotateTargetCamera()
    {
        var cameraTransform = CameraController.I.Camera.transform;
        
        while (true)
        {
            float distance = Vector3.Distance(cameraTransform.position, transform.position);
            float distance2 = Vector3.Distance(cameraTransform.position, Target.transform.position);

            Camera.depth = distance < distance2 ? -2 : -1;

            //lazy polling!
            if (CameraController.I.CameraRig.FollowTarget == null) yield break;
            
            //set target camera to rotate around like the camera rig
            var cameraLocalPosition = CameraController.I.CameraRig.FollowTarget.transform.position.To(cameraTransform.position);
            
            Target.Camera.transform.position = Target.transform.position + cameraLocalPosition;
            Target.Camera.transform.rotation= cameraTransform.rotation;

            yield return null;
        }
    }
    #endregion
    #region events

    private void OnTriggerEnter(Collider collider)
    {
        var rigidbody = collider.GetComponent<Rigidbody>();

        var velocityDirection = Vector3.Dot(rigidbody.velocity.normalized, transform.forward);

        if (velocityDirection > 0)
            collider.transform.position = Target.transform.position;
    }
    #endregion
}
