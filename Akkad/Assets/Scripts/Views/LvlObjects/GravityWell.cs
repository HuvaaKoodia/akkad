﻿using UnityEngine;
using System.Collections;

public class GravityWell : MonoBehaviour
{
    public Rigidbody Rigidbody;
    public float GravityWellForce { get { return Rigidbody.mass; } }
    public SphereCollider Collider;

    public bool DistanceAffectsForce = false;

    public void OnTriggerStay(Collider collider)
    {
        var target = collider.GetComponentInParent<GravityWellTarget>();
        if (!target) return;

        target.ApplyGravity(this);
    }

    public Vector3 GetForce(GravityWellTarget target)
    {
        var position = target.transform.position;
        var direction = (Rigidbody.position - position).normalized;

        float f = GravityWellForce;
        if (DistanceAffectsForce)
        {
            float distance = Vector2.Distance(Rigidbody.position, position);
            float radius = Collider.radius * Collider.transform.lossyScale.x;
            f *= 1f - (distance / radius);
        }

        return direction * f;
    }
}
