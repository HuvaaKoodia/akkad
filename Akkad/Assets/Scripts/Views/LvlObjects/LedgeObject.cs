﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LedgeObject : MonoBehaviour
{
    #region vars
    public bool CanClimbUp = true;
    public bool EnableForwardGrab = true, EnableRightGrab = true, EnableLeftGrab = true, EnableBackGrab = true;
    
    public BezierSpline Spline;
    
    #endregion
    #region init
    #endregion
    #region logic
    #endregion
    #region public interface

    public float GetClosestF(Vector3 worldPosition)
    {
        return Spline.GetClosestF(worldPosition);
    }

    public Vector3 GetPosition(float f)
    {
        return Spline.GetPoint(f);
    }

    #endregion
    #region private interface

    /*private void OnDrawGizmos()
    {
        DrawArrow.Gizmo(transform.position, transform.forward, Color.blue, 0.5f, 45);
    }*/

    #endregion
    #region events
    #endregion
}
