﻿using System;
using UnityEngine;

public class GravityWellTarget : MonoBehaviour
{
    #region vars
    public Delegates.Vector3Event OnGravityAppliedEvent;

    public bool SetupDefaultRigidbodyGravity = true;

    private GravityWell currentGravityWell = null;
    private Rigidbody Rigidbody;
    #endregion

    #region init

    private void Start()
    {
        if (SetupDefaultRigidbodyGravity)
        {
            Rigidbody = GetComponent<Rigidbody>();
            OnGravityAppliedEvent += ApplyRigidbodyGravity;
        }
    }

    #endregion

    #region logic

    private void Update()
    {
        if (currentGravityWell)
        {
            OnGravityAppliedEvent(currentGravityWell.GetForce(this));
        }
    }
    #endregion

    #region public interface

    public void ApplyGravity(GravityWell gravityWell)
    {
        if (currentGravityWell != gravityWell)
        {
            if (currentGravityWell == null ||
                Vector3.Distance(gravityWell.transform.position, transform.position)
                <
                Vector3.Distance(currentGravityWell.transform.position, transform.position)
                )
                currentGravityWell = gravityWell;
            else return;
        }
    }

    #endregion

    #region events

    private void ApplyRigidbodyGravity(Vector3 vector)
    {
        Rigidbody.velocity += vector * Time.deltaTime;
    }
    #endregion 
}
