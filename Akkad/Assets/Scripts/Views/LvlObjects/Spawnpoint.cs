﻿using UnityEngine;

[ExecuteInEditMode]
public class Spawnpoint : MonoBehaviour 
{
    #region vars
    //public bool UseThisSpawnPoint = false; //quick hack
    #endregion
    #region init
    #endregion
    #region logic
#if UNITY_EDITOR
    private void Update () 
	{
        //TERRIBLE HACK BUGGY CRAP!
        /*if (UseThisSpawnPoint)
        {
            var controller = FindObjectOfType<GameController>();
            
            UnityEditor.Undo.RecordObject(controller, "Changed Spawnpoint");
            controller.PlayerSpawnPoint = transform;

            //UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty(UnityEditor.SceneManagement.EditorSceneManager.GetActiveScene());
            UseThisSpawnPoint = false;
        }*/
    }
#endif
#endregion
    #region public interface
    #endregion
    #region private interface
    private void OnDrawGizmos()
    {
        DrawArrow.Gizmo(transform.position, transform.forward, Color.blue, 0.5f, 45);
    }
#endregion
#region events
#endregion
}
