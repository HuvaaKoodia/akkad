﻿using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    /// <summary>
    /// Detects objects in the scene that enter the vision triggers and have the AI.SensorTarget component.
    /// </summary>
    public class VisionDetector : MonoBehaviour
    {
        #region var
        public bool TargetsDetected { get; private set; }
        public List<SensorTarget> CurrentTargets { get; private set; }
        #endregion
        #region init
        private void Awake()
        {
            CurrentTargets = new List<SensorTarget>();
            ClearTargets();
        }
        #endregion
        #region logic
        private void FixedUpdate()
        {
            if (TargetsDetected)
            {
                ClearTargets();
            }
        }
        #endregion
        #region private interface
        private void ClearTargets()
        {
            TargetsDetected = false;
            CurrentTargets.Clear();
        }
        #endregion
        #region
        //Using TriggerEnter and Exit don't work properly with multiple colliders so TriggerStay + FixedUpdate hack will do.
        private void OnTriggerStay(Collider collider)
        {
            var target = collider.GetComponent<SensorTarget>();
            if (target == null) return;
            CurrentTargets.Add(target);
            TargetsDetected = true;
            return;
        }
        #endregion
    }
}