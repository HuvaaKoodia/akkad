﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using AI;
using System;

public class EnemyAI : MonoBehaviour 
{
    #region vars
    public float IdleWaitBeforeMovingDelay = 2f;

    private VisionSystem visionSystem;
    private EnemyMovementSystem movementSystem;
    private EnemyCombatSystem combatSystem;

    private CoroutineManager StateCM;
    private SensorTarget sensorTarget;
    private Vector3 lastSeenPosition;
    #endregion
    #region init
    private void Awake() 
	{
        StateCM = new CoroutineManager(this);

        movementSystem = GetComponent<EnemyMovementSystem>();
        visionSystem = GetComponentInChildren<VisionSystem>();
        combatSystem = GetComponent<EnemyCombatSystem>();

        visionSystem.OnTargetFoundEvent += OnTargetFound;
        visionSystem.OnTargetLost+= OnTargetLost;
    }

    private void OnEnable()
    {
        StateCM.Start(IdleCoroutine());
    }
#endregion
#region logic
	private IEnumerator IdleCoroutine() 
	{
        while (true)
        {
            yield return new WaitForSeconds(IdleWaitBeforeMovingDelay);
            movementSystem.MoveToPosition(movementSystem.GetRandomNavmeshPosition());
            movementSystem.SetMovementSpeed(0);

            //moving to destination
            while (movementSystem.Moving)
            {
                yield return null;
            }   
        }
	}

    private IEnumerator HuntTargetCoroutine()
    {   
        combatSystem.SetAttackTarget(sensorTarget);
        while (true)
        {
            if (sensorTarget)
            {
                lastSeenPosition = sensorTarget.transform.position;
                movementSystem.MoveToPosition(lastSeenPosition);
                movementSystem.SetMovementSpeed(1f);
                yield return null;
            }
            else
            {
                //go to last seen position
                movementSystem.MoveToPosition(lastSeenPosition);

                yield return null;

                //back to idle
                if (!movementSystem.Moving)
                {
                    combatSystem.ClearAttackTarget();
                    StateCM.Start(IdleCoroutine());
                }
            }
        }

        
    }
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events

    private void OnTargetFound(SensorTarget target)
    {
        if (target.TargetType == SensorTarget.Type.Player)
        {
            sensorTarget = target;
            StateCM.Start(HuntTargetCoroutine());
        }
    }

    private void OnTargetLost(SensorTarget target)
    {
        if (target.TargetType == SensorTarget.Type.Player)
        {
            sensorTarget = null;
        }
    }
#endregion
}
