﻿using System.Collections;
using UnityEngine;

public class EnemyMovementSystem : MonoBehaviour 
{
    #region vars

    public NavMeshAgent NavAgent;
    public float RandomMovementRadius = 10f;
    public Transform TempMovementTarget;
    public Rigidbody Rigidbody;

    public float WalkingSpeed = 2, RunningSpeed = 5;

    public Vector3 CenterPointOffset;

    public event Delegates.BoolEvent OnMovementEvent;
    public event Delegates.Action OnMovementUpdateEvent;
    public event Delegates.BoolEvent OnGroundEvent;
    public event Delegates.FloatEvent OnSetMovementSpeedEvent;

    public bool OnGround { get; private set; }
    public bool Moving { get; private set; }

    private CoroutineManager MovementCM;

    #endregion
    #region init

    void Start()
    {
        MovementCM = new CoroutineManager(this);
        TempMovementTarget.transform.parent = null;

        NavAgent.updatePosition = false;
        NavAgent.updateRotation = true;

        transform.position = NavAgent.nextPosition;
    }

    #endregion
    #region logic
    private void Update()
    {
        //ground raycast
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position + Vector3.up, 0.25f, -transform.up, out hitInfo, 1.1f, LayerMasks.EntityGround))
        {
            //if (FallingDown())
            {
                SetOnGround(true);
            }
        }
        else
            SetOnGround(false);
    }

    private void FixedUpdate()
    {
        transform.position = NavAgent.nextPosition;
        //Rigidbody.velocity = NavAgent.velocity;
    }
    #endregion
    #region public interface

    public Vector3 GetRandomNavmeshPosition()
    {
        var randomPos = transform.position + Random.insideUnitSphere.normalized * Helpers.Rand(NavAgent.stoppingDistance * 2, RandomMovementRadius);
        NavMeshHit hit;
        NavMesh.SamplePosition(randomPos, out hit, RandomMovementRadius, 1);
        if (hit.hit)
            return hit.position;
        return transform.position;
    }

    public void MoveToPosition(Vector3 position)
    {
        TempMovementTarget.position = position;
        NavAgent.SetDestination(position);
        MovementCM.Start(MovementCoroutine());
    }
    public void SetMovementSpeed(float speed)
    {
        OnSetMovementSpeedEvent(speed);
        NavAgent.speed = speed == 0 ? WalkingSpeed : RunningSpeed;
    }
    public IEnumerator OnStopWalking(float timeToResume)
    {
       
        NavAgent.Stop();
        Debug.Log("STOP WALIKG");
   
        yield return new WaitForSeconds(timeToResume);
        Debug.Log("Continue WALIKG");

        NavAgent.Resume();
        
    }

    #endregion
    #region private interface

    private bool FallingDown()
    {
        return Vector3.Dot(NavAgent.velocity, -transform.up) >= 0.05f;
    }

    private void SetOnGround(bool onGround)
    {
        if (onGround != OnGround)
        {
            if (OnGroundEvent != null) OnGroundEvent(onGround);
        }

        OnGround = onGround;
    }

    private IEnumerator MovementCoroutine()
    {
        Moving = true;
        
        //wait for path calculation
        float maxPendingTime = Time.time + 2f;
        while (maxPendingTime > Time.time && NavAgent.pathStatus != NavMeshPathStatus.PathComplete)
        {
            //Debug.Log("Pending!");
            yield return null;
        }

        if (maxPendingTime < Time.time)
        {
            //no path found
            Moving = false;
            yield break;
        }

        yield return null;
        if (OnMovementEvent != null) OnMovementEvent(true);

        //move on path
        while (NavAgent.remainingDistance > NavAgent.stoppingDistance && NavAgent.pathStatus == NavMeshPathStatus.PathComplete)
        {
            if (OnMovementUpdateEvent != null) OnMovementUpdateEvent();
            yield return null;
        }

        Moving = false;
        if (OnMovementEvent != null) OnMovementEvent(false);
    }

    #endregion
    #region events
    #endregion
}
