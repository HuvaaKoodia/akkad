﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EnemyCombatSystem : MonoBehaviour
{
    #region vars

    public float AttackDistance = 2, AttackMaxAngle = 15,  AttackDelay = 0.43f;

    public Delegates.Action OnAttackEvent;
    
    private CoroutineManager AttackCM;
    private EnemyMovementSystem MovementSystem;

    #endregion
    #region init

    private void Awake()
    {
        AttackCM = new CoroutineManager(this);
        MovementSystem = GetComponent<EnemyMovementSystem>();
    }

    #endregion
    #region logic
    #endregion
    #region public interface
    public void SetAttackTarget(AI.SensorTarget target)
    {
        if (target.TargetType == AI.SensorTarget.Type.Player)
        {
            var player = target.EntityTransform.GetComponent<PlayerView>();
            player.OnDeathEvent += OnTargetDeath;
            AttackCM.Start(AttackTargetCoroutine(player));
        }
    }

    public void ClearAttackTarget()
    {
        AttackCM.Stop();
    }
    #endregion
    #region private interface
    private IEnumerator AttackTargetCoroutine(PlayerView player)
    {
        while (true)
        {
            float angle = Helpers.AngleSigned(transform.forward, transform.position.ToNorm(player.transform.position), Vector3.up);
            float distance = Helpers.Distance(transform, player.transform);

            if (Mathf.Abs(angle) <= AttackMaxAngle && distance <= AttackDistance)
            {
                if (OnAttackEvent != null)
                {
                    OnAttackEvent();
                    MovementSystem.StartCoroutine("OnStopWalking", 1f);

                }
                yield return new WaitForSeconds(AttackDelay);

                distance = Helpers.Distance(transform, player.transform);

                if (Mathf.Abs(angle) <= AttackMaxAngle && distance <= AttackDistance)
                {
                    player.SetHealth(0);
                }

            }
            yield return null;
        }
    }
    #endregion
    #region events

    private void OnTargetDeath()
    {
        ClearAttackTarget();
    }

    #endregion
}
