﻿using System;
using UnityEngine;

public class EnemyAnimations : MonoBehaviour
{
    #region vars
    public Animator Animator;
    private EnemyMovementSystem movementSystem;
    private EnemyCombatSystem combatSystem;
    #endregion
    #region init
    private void Awake()
    {
        movementSystem = GetComponent<EnemyMovementSystem>();
        combatSystem = GetComponentInParent<EnemyCombatSystem>();
    }

    private void Start()
    {
        movementSystem.OnMovementEvent += OnMovement;
        movementSystem.OnMovementUpdateEvent += OnMovementUpdate;
        movementSystem.OnGroundEvent += OnGround;
        movementSystem.OnSetMovementSpeedEvent += OnMovementSpeed;
        combatSystem.OnAttackEvent += OnAttack;

        Animator.SetBool("OnGround", true);
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    private void OnMovement(bool moving)
    {
        Animator.SetBool("Moving", moving);
    }

    private void OnMovementUpdate()
    {
        Animator.SetFloat("MovementDirectionX", 0);
        Animator.SetFloat("MovementDirectionY", movementSystem.NavAgent.velocity.normalized.magnitude);
    }

    private void OnGround(bool value)
    {
        Animator.SetBool("OnGround", value);
    }
    private void OnMovementSpeed(float speed)
    {
        Animator.SetFloat("Speed", speed);
    }
    private void OnAttack()
    {
        Animator.SetTrigger("Attack");
    }

    #endregion
}
