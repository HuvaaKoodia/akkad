﻿using UnityEngine;

namespace AI
{
    /// <summary>
    /// An entity that the AI can sense in the world.
    /// </summary>
    public class SensorTarget : MonoBehaviour
    {
        #region vars
        public enum Type
        {
            Player
        }
        public Type TargetType;
        /// <summary>
        /// Target position transform. Used to locate the target in the world.
        /// </summary>
        public Transform PositionTransform;
        /// <summary>
        /// Transform of the main/manager entity. Used to get hold of the target scripts.
        /// </summary>
        public Transform EntityTransform;
        /// <summary>
        /// Is called to check if the sensor is visible or not.
        /// If no event found the VisibleByDefault boolean is used instead.
        /// </summary>
        public event Delegates.BoolAction IsVisibleEvent;

        public bool VisibleByDefault = true;
        #endregion
        #region init
        #endregion
        #region logic
        #endregion
        #region public interface
        public bool IsVisible()
        {
            if (IsVisibleEvent == null) return VisibleByDefault;
            return IsVisibleEvent();
        }
        #endregion
        #region private interface
        #endregion
        #region events
        #endregion
    }
}