using System.Collections.Generic;
using UnityEngine;

namespace AI
{
    public delegate void SensorTargetEvent(AI.SensorTarget target);
    /// <summary>
    /// Handles target acquisition based on location, priority and distance.
    /// </summary>
    public class VisionSystem : MonoBehaviour
    {
        #region vars
        public float TargetFindDelay = 0.22f;
        public float TargetLoseDelay = 0.98f;
        [Tooltip("The minimum distance difference required for the target to change to another target with the same priority.")]
        public float PriorityDistanceDeadzone = 2f;
        public bool RotateColliderBasedOnTargetElevation = false;

        public VisionDetector VisionDetector;
        public SensorTarget CurrentTarget { get; private set; }
        public Transform EntityRoot, VisionStartPoint;
        public Transform ColliderOffset;

        public event SensorTargetEvent OnTargetFoundEvent, OnTargetLost;
        public bool HasTarget { get; private set; }

        private Dictionary<SensorTarget.Type, int> sensorTargetPriorities;
        private Dictionary<SensorTarget.Type, float> sensorTargetMaxDistances;
        
        private float targetTimer = -1;
        #endregion
        #region init
        private void Awake()
        {
            //init sensor target priorities to default values
            sensorTargetPriorities = new Dictionary<SensorTarget.Type, int>();
            sensorTargetMaxDistances = new Dictionary<SensorTarget.Type, float>();

            foreach (var type in Helpers.EnumValues<SensorTarget.Type>())
            {
                sensorTargetPriorities.Add(type, 0);
                sensorTargetMaxDistances.Add(type, -1);
            }

            HasTarget = false;
        }

        #endregion
        #region logic

        private void Update()
        {
            //select best target
            bool currentTargetDetected = false;
            SensorTarget bestTarget = CurrentTarget;
            int highestPriority = -1;
            float minDistance = float.MaxValue;
            if (CurrentTarget != null)
            {
                highestPriority = sensorTargetPriorities[CurrentTarget.TargetType];
                minDistance = Helpers.DistanceOnHorizontalPlane(EntityRoot.position, CurrentTarget.PositionTransform.position);
            }

            for (int i = 0; i < VisionDetector.CurrentTargets.Count; i++)
            {
                var target = VisionDetector.CurrentTargets[i];
                if (!CanSeeTarget(target)) continue;

                //check target max distance
                float maxDistance = sensorTargetMaxDistances[target.TargetType];
                float distance = Helpers.DistanceOnHorizontalPlane(EntityRoot.position, target.PositionTransform.position);

                if (maxDistance > 0 && distance > maxDistance) continue;

                //continue seeing current target
                if (CurrentTarget == target)
                {
                    currentTargetDetected = true;
                    continue;
                }

                //check priority and distance deadzone
                int priority = sensorTargetPriorities[target.TargetType];
                if (priority >= highestPriority)
                {
                    //reset distance when a higher priority target is spotted
                    if (priority > highestPriority) minDistance = float.MaxValue;

                    if (distance + PriorityDistanceDeadzone < minDistance)
                    {
                        minDistance = distance;
                        highestPriority = priority;
                        bestTarget = target;
                    }
                }
            }

            //change target
            if (bestTarget != CurrentTarget)
            {
                CurrentTarget = bestTarget;
                //reset delay values
                currentTargetDetected = true;
                targetTimer = -1;
                HasTarget = false;
            }

            //find and lose target with a delay
            if (CurrentTarget != null)
            {
                if (!HasTarget)
                {
                    //find target after a delay
                    if (currentTargetDetected)
                    {
                        if (targetTimer == -1)
                        {
                            targetTimer = Time.time + TargetFindDelay;
                        }
                        if (targetTimer < Time.time)
                        {
                            AddTarget(CurrentTarget);
                            targetTimer = -1;
                        }
                    }
                    else
                    {
                        targetTimer = -1;
                        CurrentTarget = null;
                    }
                }
                else
                {
                    if (currentTargetDetected)
                    {
                        targetTimer = -1;

                        //rotate collider up and down based on target elevation
                        if (RotateColliderBasedOnTargetElevation)
                        {
                            float angle = Helpers.AngleSigned(EntityRoot.forward, EntityRoot.ToNorm(CurrentTarget.EntityTransform), EntityRoot.right);

                           // Debug.Log("Angle" + angle);

                            ColliderOffset.localRotation = Quaternion.AngleAxis(angle, Vector3.right);
                        }
                    }
                    else 
                    {
                        //lose target after a delay
                        if (targetTimer == -1)
                        {
                            targetTimer = Time.time + TargetLoseDelay;
                        }
                        if (targetTimer < Time.time)
                        {
                            RemoveTarget(CurrentTarget);
                            targetTimer = -1;

                            //reset collider back to normal
                            if (RotateColliderBasedOnTargetElevation)
                            {
                                ColliderOffset.localRotation = Quaternion.identity;
                            }
                        }
                    }

                }
            }
        }
        #endregion
        #region public interface
        /// <summary>
        /// Calls the lose target event and sets the current target to null.
        /// </summary>
        public void ResetCurrentTarget()
        {
            RemoveTarget(CurrentTarget);
            CurrentTarget = null;
        }
        /// <summary>
        /// Sets target priority and max distance values uses in target acquisition.
        /// </summary>
        /// <param name="priority">A non-negative integer. Bigger -> more important</param>
        /// <param name="maxDistance">The maximum distance for this target to be detected</param>
        public void SetTargetPriority(SensorTarget.Type targetType, int priority, float maxDistance = -1)
        {
            if (priority < 0) priority = 0;
            sensorTargetPriorities[targetType] = priority;
            sensorTargetMaxDistances[targetType] = maxDistance;
        }

        public bool CanSeeTarget(SensorTarget target)
        {
            if (target == null) return false;
            return target.IsVisible() && CanSeeTarget(target.transform);
        }

        public bool CanSeeTarget(Transform target)
        {
            Vector3 position = VisionStartPoint.position;
            Vector3 direction = (target.position + Vector3.up) - position;
            if (Physics.Raycast(position, direction, direction.magnitude, LayerMasks.AIVisionBlockMask))
            {
                Debug.DrawRay(position, direction, Color.red);
                return false;
            }

            Debug.DrawRay(position, direction, Color.green);
            return true;
        }

        #endregion
        #region private interface
        private void AddTarget(SensorTarget target)
        {
            if (OnTargetFoundEvent != null) OnTargetFoundEvent(target);
            HasTarget = true;
        }

        private void RemoveTarget(SensorTarget target)
        {
            if (OnTargetLost != null) OnTargetLost(target);
            HasTarget = false;
        }
        #endregion
        #region events
        #endregion
    }
}