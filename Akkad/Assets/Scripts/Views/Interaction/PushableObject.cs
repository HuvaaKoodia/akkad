﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PushableObject : MonoBehaviour 
{
    #region vars
    public Rigidbody Rigidbody;
#endregion
    #region init
    private void Start () 
	{
        Rigidbody.mass = 1000;
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    public void StartPush()
    {
        Rigidbody.mass = 1;
    }

    public void StopPush()
    {
        Rigidbody.mass = 1000;
    }
    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
