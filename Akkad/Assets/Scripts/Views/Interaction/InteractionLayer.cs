﻿using System.Collections.Generic;
using UnityEngine;

namespace InteractionSystem
{
    public enum TypeID
    {
        Kinetic = 0,
        Energy,
		Activate,
        Collectable,
        _Amount
    }
    [System.Serializable]
    public class Data
    {
        public TypeID Type;
        public int IntValue;
        public float FloatValue;
        public Transform Entity;

		public Data(TypeID type)
		{
			Type = type;
		}
    }


    public delegate void InteractionEvent(Data data);

    public class InteractionLayer : MonoBehaviour
    {
        #region vars
        public Data Data;
        public event InteractionEvent OnInputEvent, OnOutputEvent;
        public bool DisableInput = false;
        public bool DisablePhysicsOutput = false;
        public string InteractionMessage;

        #endregion
        #region init
        #endregion
        #region logic
        #endregion
        #region public interface
        public void ReceiveInput(Data interactionData)
        {
            if (DisableInput) return;
            if (OnInputEvent != null) OnInputEvent(interactionData);
        }
        #endregion
        #region private interface
		private void SendOutputTo(InteractionLayer interactionLayer)
		{
			interactionLayer.ReceiveInput(Data);
			if (OnOutputEvent != null) OnOutputEvent(interactionLayer.Data);
		}
		
        #endregion
        #region events
        private void OnCollisionEnter(Collision collision)
        {
            if (DisablePhysicsOutput) return;
            var interactionLayer = collision.collider.GetComponentInParent<InteractionLayer>();
            if (interactionLayer) SendOutputTo(interactionLayer);
        }
        
        private void OnTriggerEnter(Collider collider)
        {
            if (DisablePhysicsOutput) return;
            var interactionLayer = collider.GetComponent<InteractionLayer>();
            if (interactionLayer) SendOutputTo(interactionLayer);
        }
        #endregion
    }
}