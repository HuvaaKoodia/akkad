﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;

public class DialogDatabase : MonoBehaviour
{

    public string DataFolder = "DialogueData/";

    public class DialogueLink
    {
        public string ID, Text;
    }

    public class DialogueData
    {
        public string ID, CharacterID, Text, Link = null;
        public List<DialogueLink> Links;
    }

    private class NameData
    {
        public string ID = "", Name = "";
    }

    #region vars
    public static DialogDatabase I;

    private Dictionary<string, string> nameIDTable = new Dictionary<string, string>();
    private Dictionary<string, DialogueData> dialogueDataTable = new Dictionary<string, DialogueData>();
    #endregion
    #region init
    void Awake()
    {
        if (I != null)
        {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(gameObject);

        I = this;

        //read dialogue files
        var dialogueDataAssets = Resources.LoadAll<TextAsset>(DataFolder);

        foreach (var item in dialogueDataAssets)
        {
            //character name parsing
            if (item.name == "CharacterNames") //lil' hack
            {
                var dataList = JsonConvert.DeserializeObject(item.text, typeof(List<NameData>)) as List<NameData>;

                foreach (var data in dataList)
                {
                    string id = data.ID;
                    if (this.nameIDTable.ContainsKey(id))
                    {
                        Debug.LogFormat("CharacterID {0} already exists", id);
                        continue;
                    }
                    nameIDTable.Add(id, data.Name);
                }

                continue;
            }

            //dialogue data parsing
            {
                var dataList = JsonConvert.DeserializeObject(item.text, typeof(List<DialogueData>)) as List<DialogueData>;

                foreach (var data in dataList)
                {
                    string id = data.ID;
                    if (this.dialogueDataTable.ContainsKey(id))
                    {
                        Debug.LogFormat("DialogueData {0} already exists", id);
                        continue;
                    }

                    this.dialogueDataTable.Add(id, data);
                }
            }
        }
    }

    #endregion
    #region logic
    #endregion
    #region public interface   
    public string GetName(string characterID)
    {
        if (!nameIDTable.ContainsKey(characterID)) return "NOT FOUND!";

        return nameIDTable[characterID];
    }

    public DialogueData GetDialog(string dialogueID)
    {
        return dialogueDataTable[dialogueID];
    }
    #endregion
    #region private interface
    #endregion
    #region events

    #endregion
}
