﻿using UnityEngine;

/// <summary>
/// Contains game specific layer bit masks and indices.
/// </summary>
public class LayerMasks
{
    public static readonly int Ground = 1 << LayerMask.NameToLayer("Ground");
    public static readonly int Wall = 1 << LayerMask.NameToLayer("Wall");
    public static readonly int Button = 1 << LayerMask.NameToLayer("Button");
    public static readonly int Ledge = 1 << LayerMask.NameToLayer("Ledge");
    public static readonly int Prop = 1 << LayerMask.NameToLayer("Prop");
    public static readonly int Water = 1 << LayerMask.NameToLayer("Water");
    public static readonly int Interactable = 1 << LayerMask.NameToLayer("Interactable");
    
    public static readonly int EntityGround = Wall | Ground | Prop | Interactable;
    public static readonly int ObstacleBlockCamera = Wall | Ground;
    public static readonly int AIVisionBlockMask = Wall | Ground | Prop;

}