﻿
/// <summary>
/// Contains game specific unity tags and other data labels.
/// Make sure these match with the ones in game.
/// </summary>
public class GameTags
{
    //Unity tags
    public const string Player = "Player";
}
