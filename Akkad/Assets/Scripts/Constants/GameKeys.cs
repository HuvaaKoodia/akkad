﻿/// <summary>
/// Contains game specific input button names.
/// </summary>
public class GameKeys
{
	//Keys
    public const string Fire1 = "Fire1";
    public const string Fire2 = "Fire2";
	public const string Jump = "Jump";
	public const string Sprint = "Sprint";
	public const string Interact = "Interact";
    public const string Escape = "Escape";

    //Axes
    public const string HorizontalAxis = "HorizontalAxis";
	public const string VerticalAxis = "VerticalAxis";
    public const string MouseXAxis = "Mouse X";
    public const string MouseYAxis = "Mouse Y";
}
