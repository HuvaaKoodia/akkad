﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

public class DisableMaterialImport : AssetPostprocessor
{
    #region vars
    #endregion
    #region init
    #endregion
    #region logic
    #endregion
    #region public interface
    public void OnPreprocessModel()
    {
        var modelImporter = assetImporter as ModelImporter;
        modelImporter.importMaterials = false;
    }
#endregion
#region private interface
#endregion
#region events
#endregion
}
