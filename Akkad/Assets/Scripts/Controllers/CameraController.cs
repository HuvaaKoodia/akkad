﻿using UnityEngine;

namespace Cameras
{
    public class CameraController : MonoBehaviour
    {
        #region vars
        public static CameraController I;

        public CameraView Camera;
        public Rigs.TargetOrbitCamera CameraRig;
        #endregion
        #region init
        private void Awake()
        {
            I = this;
        }

        private void Start()
        {
            //player setup
            var player = GameController.I.Player;
            player.OnDeathEvent += OnPlayerDeath;

            //camera setup
            CameraRig.Camera = Camera.Camera;
            CameraRig.LookTarget = player.CameraLookTarget;
            CameraRig.FollowTarget = player.CameraFollowTarget;

            Camera.LookTarget = player.CameraLookTarget;
            Camera.FollowTarget = CameraRig.CameraPosition;


            //start position hack
            CameraRig.transform.position = player.CameraFollowTarget.position;
            Camera.transform.position = player.CameraFollowTarget.position;

            CameraRig.FixedUpdate();
            Camera.FixedUpdate();
        }
        #endregion
        #region logic
        #endregion
        #region public interface
        #endregion
        #region private interface
        #endregion
        #region events
        private void OnPlayerDeath()
        {
            Camera.LookTarget = null;
            CameraRig.enabled = false;
            CameraRig.FollowTarget = null;
            CameraRig.LookTarget = null;
        }
        #endregion
    }
}