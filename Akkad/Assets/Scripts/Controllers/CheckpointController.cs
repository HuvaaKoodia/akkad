﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheckpointController : MonoBehaviour
{
    #region vars
    public static CheckpointController I;

    private static int currentCheckpointIndex = -1;
    #endregion
    #region init
    private void Awake()
    {
        I = this;

        if (currentCheckpointIndex != -1)
        {
            //find all checkpoints and spawn player at the correct one
            var checkpoints = FindObjectsOfType<Checkpoint>();

            foreach (var checkpoint in checkpoints)
            {
                if (checkpoint.Index == currentCheckpointIndex)
                {
                    //a clever hack for once!
                    GameController.I.PlayerSpawnPoint = checkpoint.Spawnpoint.transform;
                    break;
                }
            }
        }

        SaveController.SaveCheckpoint(currentCheckpointIndex);
        SaveController.SaveCurrentLevel();
    }

    #endregion
    #region logic
    #endregion
    #region public interface

    public static void ReloadFromCheckpoint()
    {
        GameController.ReloadLevel();
    }

    public static void SetCurrentCheckpointIndex(int index)
    {
        currentCheckpointIndex = index;
        SaveController.SaveCheckpoint(currentCheckpointIndex);
    }

    public static void ResetCurrentCheckpointIndex()
    {
        SetCurrentCheckpointIndex(-1);
    }

    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
