﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class MovieController : MonoBehaviour
{
#region vars
	public static MovieController I;
    public Image Image, FadeImage;
    public AudioSource Audio;

    private MovieTexture movieTexture;
    private CoroutineManager movieCM;
#endregion
#region init
	private void Awake()
	{
        if (I)
        {
            DestroyImmediate(this);
            return;
        }

        FadeImage.enabled = false;
        Image.enabled = false;

        I = this;
        GameObject.DontDestroyOnLoad(gameObject);

        movieCM = new CoroutineManager(this);
        movieTexture = (MovieTexture)Image.material.mainTexture;
    }
    #endregion
    #region logic
#if UNITY_EDITOR
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (movieTexture.isPlaying)
                StopMovie();
            else 
                PlayMovie((System.Action)null);
        }
    }
#endif
#endregion
    #region public interface
    public void PlayMovie(System.Action OnMovieEndEvent)
    {
        Time.timeScale = 0;
        Image.enabled = true;

        FadeImage.enabled = true;
        movieTexture.Play();
        Audio.Play();

        movieCM.Start(StopMovieCoroutine(movieTexture.duration, OnMovieEndEvent));
    }

    public void PlayMovie(string nextLevel)
    {
        this.nextLevel = nextLevel;
        PlayMovie(NextLvl);
    }

    public void StopMovie()
    {
        Time.timeScale = 1;
        Image.gameObject.SetActive(false);
        movieTexture.Stop();
        Audio.Stop();
        movieCM.Stop();
    }

    private string nextLevel;

    /// <summary>
    /// The next level method and coroutine are here because they have to survive the level change. They are not here because it makes any sense, but whatever...
    /// </summary>
    public void NextLvl()
    {
        StartCoroutine(NextLvlCoroutine(nextLevel));
    }

    #endregion
    #region private interface

    public IEnumerator NextLvlCoroutine(string nextLevel)
    {
        var operation = GameController.I.NextLevel(nextLevel);
        while (!operation.isDone)
        {
            yield return null;
        }

        MovieController.I.FadeImage.enabled = false;
    }


    private IEnumerator StopMovieCoroutine(float duration, System.Action OnMovieEndEvent)
    {
        while (duration > 0)
        {
            duration -= Time.unscaledDeltaTime;
            yield return null;
        }
        StopMovie();
        if (OnMovieEndEvent != null) OnMovieEndEvent();
    }
    #endregion
    #region events
    #endregion
}
