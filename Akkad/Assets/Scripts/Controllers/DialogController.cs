﻿using UnityEngine;
using System.Collections;
using System;

public class DialogController : MonoBehaviour {

    #region vars
    public static DialogController I;

    public event Delegates.StringEvent OnDialogStartEvent;
    public event Delegates.StringEvent OnDialogEndEvent;
    public event Delegates.DialogDataEvent OnDialogChangedEvent;

    public bool DialogOn { get; private set; }

    public GameObject GameoverPanel;

    private int dialogOptionInput = -1;
    private bool dialogContinueInput = true;
    #endregion
    #region init

    private void Awake()
    {
        I = this;
    }

    private void Start()
    {
        GUIController.I.OnDialogOptionChosenEvent += OnDialogOptionChosen;
        InputController.I.OnInteractInput += OnInteractInput;

        GameController.I.Player.OnDeathEvent += OnPlayerDeath;

        GameoverPanel.SetActive(false);
    }

    private void OnPlayerDeath()
    {
        GameoverPanel.SetActive(true);
    }

    #endregion
    #region logic
    #endregion
    #region public interface

    public void StartDialog(string DialogName)
    {
        DialogOn = true;
        GameController.I.SetCursorVisible(true);
        StartCoroutine(DialogueCoroutine(DialogName));
    }

    #endregion
    #region private interface

    private IEnumerator DialogueCoroutine(string dialogName)
    {
        string startDialogName = dialogName;
        if (OnDialogStartEvent != null) OnDialogStartEvent(startDialogName);

        while (!string.IsNullOrEmpty(dialogName))
        {
            var data = DialogDatabase.I.GetDialog(dialogName);

            if (OnDialogChangedEvent != null) OnDialogChangedEvent(data);

            //direct link
            if (data.Links == null || data.Links.Count == 0)
            {
                //wait for input
                dialogContinueInput = false;
                while (!dialogContinueInput)
                {
                    yield return null;
                }

                dialogName = data.Link;

                yield return null;
            }
            else
            {
                dialogOptionInput = -1;

                //wait for input
                while (dialogOptionInput == -1) yield return null;

                dialogName = data.Links[dialogOptionInput].ID;
            }
                
        }

        DialogOn = false;
        GameController.I.SetCursorVisible(false);
        if (OnDialogEndEvent != null) OnDialogEndEvent(startDialogName);
    }
    #endregion
    #region events

    private void OnDialogOptionChosen(int value)
    {
        dialogOptionInput = value;
    }

    private void OnInteractInput()
    {
        dialogContinueInput = true;
    }

    #endregion
}
