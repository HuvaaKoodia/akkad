﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class InputController : MonoBehaviour
{
    #region vars
    public static InputController I;

    public event Delegates.DoubleFloatEvent OnAxisInput;
    public event Delegates.DoubleFloatEvent OnMouseAxisInput;
    public event Delegates.Action OnJumpInput, OnJumpUpInput;
    public event Delegates.BoolEvent OnSprintInput;
    public event Delegates.Action OnInteractInput;
    public event Delegates.Action OnEscapePressed;
    public event Delegates.Action OnCameraLookAroundInput, OnCameraLookAroundUpInput;
    #endregion
    #region init
    private void Awake()
    {
        I = this;
    }
    #endregion
    #region logic

    public void Update()
    {
        if (OnMouseAxisInput != null)
        {
            float axis = Input.GetAxis(GameKeys.MouseXAxis);
            float axis2 = Input.GetAxis(GameKeys.MouseYAxis);

            OnMouseAxisInput(axis, axis2);
        }

        if (OnAxisInput != null)
        {
            float horizontalAxis = Input.GetAxis(GameKeys.HorizontalAxis);
            float verticalAxis = Input.GetAxis(GameKeys.VerticalAxis);

           // float horizontalAxisR = Input.GetAxisRaw(GameKeys.HorizontalAxis);
            //float verticalAxisR = Input.GetAxisRaw(GameKeys.VerticalAxis);

            OnAxisInput(horizontalAxis, verticalAxis);
        }

        if (OnJumpInput != null)
        {
            if (Input.GetButton(GameKeys.Jump))
            {
                OnJumpInput();
            }
        }

        if (OnJumpUpInput != null)
        {
            if (Input.GetButtonUp(GameKeys.Jump))
            {
                OnJumpUpInput();
            }
        }

        if (OnSprintInput != null)
        {
            //keyboard
            if (Input.GetButtonDown(GameKeys.Sprint))
            {
                OnSprintInput(true);
            }
            else if (Input.GetButtonUp(GameKeys.Sprint))
            {
                OnSprintInput(false);
            }

            //controller axis
            float axis = Mathf.Abs(Input.GetAxis(GameKeys.Sprint));

            OnSprintInput(axis > 0);
        }

        if (OnInteractInput != null)
        {
            if (Input.GetButtonDown(GameKeys.Interact))
            {
                OnInteractInput();
            }
        }

        if (OnEscapePressed != null)
        {
            if (Input.GetButtonDown(GameKeys.Escape))
            {
                OnEscapePressed();
            }
        }

        if (OnCameraLookAroundInput != null)
        {
            if (Input.GetMouseButton(1))
            {
                OnCameraLookAroundInput();
            }
        }

        if (OnCameraLookAroundUpInput != null)
        {
            if (Input.GetMouseButtonUp(1))
            {
                OnCameraLookAroundUpInput();
            }
        }
    }
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
