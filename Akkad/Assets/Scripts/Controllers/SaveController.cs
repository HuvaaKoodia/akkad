﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SaveController
{
    #region vars
    private const string LevelSaveKey = "LevelIndex", CheckpointSaveKey = "Checkpoint";
    #endregion
    #region init

    #endregion
    #region logic
    #endregion
    #region public interface
    public static bool HasSavedLevel()
    {
        return PlayerPrefs.HasKey(LevelSaveKey);
    }

    public static void SaveCurrentLevel()
    {
        //quick hack using level build index
        int buildIndex = SceneManager.GetActiveScene().buildIndex;
        PlayerPrefs.SetInt(LevelSaveKey, buildIndex);
    }

    public static void SaveCheckpoint(int checkpointIndex)
    {
        PlayerPrefs.SetInt(CheckpointSaveKey, checkpointIndex);
    }


    public static void LoadSavedLevel()
    {
        int levelIndex = PlayerPrefs.GetInt(LevelSaveKey);
        int checkpointIndex = PlayerPrefs.GetInt(CheckpointSaveKey);

        CheckpointController.SetCurrentCheckpointIndex(checkpointIndex);

        SceneManager.LoadScene(levelIndex);
    }
    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
