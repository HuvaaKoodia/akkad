﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Cameras.Rigs;
using Cameras;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    #region vars
    public static GameController I;

	public Transform PlayerSpawnPoint;
    public PlayerView PlayerPrefab;

    public PlayerView Player { get; private set; }
    #endregion
    #region init
    private void Awake()
    {
        I = this;

        //TapsanEbinKommentti
        //QualitySettings.vSyncCount = 0;  // VSync must be disabled
        //Application.targetFrameRate = 30;
    }

    private void Start()
    {
        //player setup
        Player = Instantiate(PlayerPrefab, PlayerSpawnPoint.position, PlayerSpawnPoint.rotation) as PlayerView;
        Player.OnDeathEvent += OnPlayerDeath;

        //events
        InputController.I.OnEscapePressed += OnEscapePressed;
        InputController.I.OnInteractInput += OnInteract;

        if (DialogController.I)
        {
            DialogController.I.OnDialogStartEvent += OnDialogStart;
            DialogController.I.OnDialogEndEvent += OnDialogEnd;
        }

        SetCursorVisible(false);
    }

    private void OnInteract()
    {
        if (Player.Dead)
        {
            CheckpointController.ReloadFromCheckpoint();
            return;
        }
    }

    #endregion
    #region logic

    private void Update()
    {
        //reload
        if (Input.GetKeyDown(KeyCode.R))
        {
            ReloadLevel();
            return;
        }

        //skip to

        if (Input.GetKeyDown(KeyCode.F1))
        {
            CheckpointController.ResetCurrentCheckpointIndex();
            LoadLevel(1);
            return;
        }
        if (Input.GetKeyDown(KeyCode.F2))
        {
            CheckpointController.ResetCurrentCheckpointIndex();
            LoadLevel(2);
            return;
        }
        if (Input.GetKeyDown(KeyCode.F3))
        {
            CheckpointController.ResetCurrentCheckpointIndex();
            LoadLevel(3);
            return;
        }
        if (Input.GetKeyDown(KeyCode.F4))
        {
            CheckpointController.ResetCurrentCheckpointIndex();
            LoadLevel(4);
            return;
        }
        if (Input.GetKeyDown(KeyCode.F5))
        {
            CheckpointController.ResetCurrentCheckpointIndex();
            LoadLevel(5);
            return;
        }

#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.L))
        {
            var visible = Cursor.lockState == CursorLockMode.Locked;
            SetCursorVisible(visible);
        }
#endif

    }

    #endregion
    #region public interface
    public void Spawnplayer(Spawnpoint spawnpoint)
    {

    }

    public AsyncOperation NextLevel(string NextLevel)
    {
        CheckpointController.ResetCurrentCheckpointIndex();
        return SceneManager.LoadSceneAsync(NextLevel);    
    }

    public static void ReloadLevel()
    {
        LoadLevel(SceneManager.GetActiveScene().buildIndex);
    }

    public static void LoadLevel(int buildIndex)
    {
        Physics.gravity = Vector3.down * 9.81f;
        SceneManager.LoadScene(buildIndex);
    }

    public void SetCursorVisible(bool visible)
    {
        Cursor.lockState = visible ? CursorLockMode.None : CursorLockMode.Locked;
        Cursor.visible = visible;

        Player.MovementSystem.DisableRotationHack = visible;
    }
    public void DisablePlayerMovement()
    {
        Player.DisableInput();
        Player.MovementSystem.SetMoving(false);
        Player.SensorTarget.VisibleByDefault = false;
    }
    public void EnablePlayerMovement()
    {
        Player.EnableInput();
        Player.MovementSystem.SetMoving(true);
        Player.SensorTarget.VisibleByDefault = true;
    }

    #endregion
    #region private interface
    #endregion
    #region events

    private void OnPlayerDeath()
    {

    }

    private void OnEscapePressed()
    {
        //Application.Quit();
        SetCursorVisible(true);
        SceneManager.LoadScene(0);
        MusicController.I.StopMusicTrack();
    }

    private void OnDialogStart(string dialogName)
    {
        DisablePlayerMovement();
    }

    private void OnDialogEnd(string dialogName)
    {
        EnablePlayerMovement();
    }
    #endregion
}
