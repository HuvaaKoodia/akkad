﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIController : MonoBehaviour
{
#region vars
	public static GUIController I;

    public Text InteractionText;
    public Text OrbText;
    public CanvasGroup DialogPanel;
    public GameObject DialogButtonsPanel;
    public Text DialogText;
    public List<Button> DialogOptionButtons;

    public event Delegates.IntEvent OnDialogOptionChosenEvent;

    int orbsIndex = 0;
    #endregion
    #region init
    private void Awake()
	{
		I = this;

        DialogPanel.alpha = 0;
    }

    private void Start()
    {
        GameController.I.Player.OnDeathEvent += OnPlayerDeath;
        GameController.I.Player.OnChangeHealthEvent += OnPlayerHealthChanged;
        GameController.I.Player.InteractionSystem.OnInteractionRangeEnterEvent += OnInteractionRange;
        GameController.I.Player.InteractionSystem.OnInteractionRangeExitEvent += OnInteractionRange;

        //orb hack!
        if (lvl1Controller.I != null) lvl1Controller.I.OnOrbCollectedEvent += OnOrbCollected;

        if (DialogController.I)
        {
            DialogController.I.OnDialogStartEvent += OnDialogStartEvent;
            DialogController.I.OnDialogEndEvent += OnDialogEndEvent;
            DialogController.I.OnDialogChangedEvent += OnDialogChangedEvent;
        }

        OrbText.gameObject.SetActive(false);
    }

    private void OnOrbCollected()
    {
        OrbText.gameObject.SetActive(true);
        OrbText.text = "" + (++orbsIndex);
    }

    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events

    private void OnInteractionRange(string text)
    {
        InteractionText.text = text; 
    }

    private void OnPlayerDeath()
    {

    }

    private void OnPlayerHealthChanged(int health)
    {
        //HealthText.text = "";// + health;
    }

    private void OnDialogStartEvent(string text)
    {
        DialogPanel.alpha = 1;
    }

    private void OnDialogChangedEvent(DialogDatabase.DialogueData data)
    {
        DialogText.text = DialogDatabase.I.GetName(data.CharacterID) + ":\n" + data.Text;

        if (data.Links != null)
        {
            DialogButtonsPanel.SetActive(true);
            for (int i = 0; i < DialogOptionButtons.Count; i++)
            {
                if (data.Links.Count <= i)
                {
                    DialogOptionButtons[i].gameObject.SetActive(false);
                    continue;
                }

                DialogOptionButtons[i].gameObject.SetActive(true);
                var link = data.Links[i];
                DialogOptionButtons[i].GetComponentInChildren<Text>().text = link.Text;
            }
        }
        else DialogButtonsPanel.SetActive(false);
    }

    public Image KeyboardControls, PadControls;

    public void ShowControls(float delay)
    {
        StartCoroutine(ShowControlsCoroutine(delay));
    }

    private IEnumerator ShowControlsCoroutine(float delay)
    {
        bool hasPad = Input.GetJoystickNames().Length > 0;

        if (hasPad) PadControls.gameObject.SetActive(true);
        else KeyboardControls.gameObject.SetActive(true);

        yield return new WaitForSeconds(delay);

        PadControls.gameObject.SetActive(false);
        KeyboardControls.gameObject.SetActive(false);
    }

    private void OnDialogEndEvent(string dialogName)
    {
        DialogPanel.alpha = 0;
    }

    public void OnDialogOptionButtonClicked(int value)
    {
        if (OnDialogOptionChosenEvent != null) OnDialogOptionChosenEvent(value);
    }
    #endregion
}
