﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class KillTrigger : MonoBehaviour 
{
    #region vars
    #endregion
    #region init
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    private void OnTriggerEnter(Collider collider)
    {
        var player = collider.GetComponent<PlayerView>();
        if(player) player.SetHealth(0);


    }
    #endregion
}
