﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class LvlPortal : MonoBehaviour
{
    public string NextLevel;

    private void OnTriggerEnter(Collider other)
    {
        MovieController.I.PlayMovie(NextLevel);
        MusicController.I.MusicVolumeDec(1f);
    }
}
