﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StartVelocity : MonoBehaviour 
{
    #region vars
    public Vector3 Velocity = Vector3.one;
#endregion
#region init
	private void Start () 
	{
        GetComponent<Rigidbody>().velocity = Velocity;
	}
#endregion
#region logic
#endregion
#region public interface
#endregion
#region private interface
#endregion
#region events
#endregion
}
