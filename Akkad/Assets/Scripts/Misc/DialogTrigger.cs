﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class DialogTrigger : MonoBehaviour {

    #region vars
    public string DialogName = null;
    public UnityEvent OnPlayerEnter;

    #endregion
    #region init
    void Start()
    {
    }
    #endregion
    #region logic
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            StartDialog();
        }
    }
    #endregion
    #region public interface   
    public void StartDialog()
    {
        OnPlayerEnter.Invoke();

        if(!string.IsNullOrEmpty(DialogName))
        {
            DialogController.I.StartDialog(DialogName);
        }
    }

    public void UpdateDialogName(string NewDialogText)
    {
        DialogName = NewDialogText;
    }


    
    #endregion
    #region private interface
    #endregion
    #region events

  
    #endregion
}
