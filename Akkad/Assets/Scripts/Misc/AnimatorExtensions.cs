﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public static class AnimatorExtensions
{
    /// <summary>
    /// Returns a single AnimatorStateListener object with the specified name from the animator.
    /// Null if none found.
    /// </summary>
    public static AnimatorStateListener GetListener(this Animator animator, string name)
    {
        var behaviours = animator.GetBehaviours<AnimatorStateListener>();
        var behaviour = behaviours.FirstOrDefault(b => b.StateName == name);
#if UNITY_EDITOR
        //VIKING
        //   if (behaviour == null) Debug.LogError(string.Format("No AnimatorStateListener called {0} found in {1}", name, animator.name));
#endif
        return behaviour;
    }

    /// <summary>
    /// Returns all AnimatorStateListener objects with the specified name from the animator.
    /// Empty list if none found.
    /// </summary>
    public static List<AnimatorStateListener> GetListeners(this Animator animator, string name)
    {
        var behaviours = animator.GetBehaviours<AnimatorStateListener>();
        var states = behaviours.Where(b => b.StateName == name).ToList();
#if UNITY_EDITOR
        if (states.Count == 0) Debug.LogError(string.Format("No AnimatorStateListeners called {0} found in {1}", name, animator.name));
#endif
        return states;
    }
}
