﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyAfterPS : MonoBehaviour 
{
    #region vars
    public ParticleSystem PS;
#endregion
#region init
	private IEnumerator Start () 
	{
        yield return null;
        while (PS.isPlaying && PS.particleCount > 0) yield return null;

        Destroy(gameObject);
	}
#endregion
#region logic
#endregion
#region public interface
#endregion
#region private interface
#endregion
#region events
#endregion
}
