﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DestroyAfterDelay : MonoBehaviour 
{
    #region vars
    public float Delay = 1f;
#endregion
#region init
	private IEnumerator Start () 
	{
        yield return new WaitForSeconds(Delay);

        Destroy(gameObject);
	}
#endregion
#region logic
#endregion
#region public interface
#endregion
#region private interface
#endregion
#region events
#endregion
}
