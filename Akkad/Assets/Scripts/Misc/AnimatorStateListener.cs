﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Allows listening for animator state enter and exit events.
/// StateMachineBehaviours are completely reset when disabling the animator. 
/// Set the events in the OnEnable method to work around this.
/// 
/// Use the Animator.GetListener and Animator.GetListeners extensions to access StateListeners in the Animators
/// </summary>
public class AnimatorStateListener : StateMachineBehaviour
{
    public string StateName;
    public UnityEvent OnEnterEvent { get { return onEnterEvent; } set { onEnterEvent = value; } }
    public UnityEvent OnExitEvent { get { return onExitEvent; } set { onExitEvent = value; } }

    [SerializeField]
    private UnityEvent onEnterEvent = new UnityEvent();
    [SerializeField]
    private UnityEvent onExitEvent = new UnityEvent();

    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnEnterEvent.Invoke();
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        OnExitEvent.Invoke();
    }
}
