﻿using UnityEngine;
using System.Collections;

public class LookAtScript : MonoBehaviour {
	#region vars
	public Transform Target;
    public bool UseTargetUpVector = false;
    public float RotationSpeed   =2f;

	#endregion
	#region init
	public void start()
	{

	}
	#endregion
	#region logic

	public void Update()
	{
        if (Target != null)
        {
            Vector3 relativePos = Target.position - transform.position;
            relativePos.y = 0;
            Quaternion rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(relativePos, UseTargetUpVector ? Target.up : Vector3.up), Time.deltaTime * RotationSpeed);
            transform.rotation = rotation;
        }
	}

	#endregion
	#region public interface
	#endregion
	#region private interface
	#endregion
	#region events
	#endregion
}
