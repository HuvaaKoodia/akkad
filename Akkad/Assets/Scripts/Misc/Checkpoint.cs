﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Checkpoint : MonoBehaviour 
{
    #region vars
    public int Index = 1;
    public Spawnpoint Spawnpoint;
    #endregion
    #region init
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    private void OnTriggerEnter(Collider collider)
    {
        CheckpointController.SetCurrentCheckpointIndex(Index);
    }
    #endregion
}
