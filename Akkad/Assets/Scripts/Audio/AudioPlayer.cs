﻿using UnityEngine;
using UnityEngine.Audio;

public class AudioPlayer : MonoBehaviour
{
    #region vars
    public AudioClip[] AudioClips;
    public AudioMixerGroup AudioMixerGroup;
    #endregion
    #region init
    #endregion
    #region logic
    #endregion
    #region public interface
    public void Play2DSound(int channel = 0)
    {
        AudioController.I.PlayAudio(Helpers.Rand(AudioClips), AudioMixerGroup, channel);
    }

    public void Play3DSound(int channel = 0)
    {
        AudioController.I.PlayAudio(transform.position, Helpers.Rand(AudioClips), AudioMixerGroup, channel);
    }
    #endregion
    #region private interface
    #endregion
    #region events
    #endregion
}
