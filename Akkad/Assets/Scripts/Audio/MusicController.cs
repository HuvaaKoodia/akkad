using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System;

public class MusicController : MonoBehaviour
{
    public static MusicController I;

    private int trackIndex = -1;
    //public AudioClip AmbientTrack;
    //public AudioSource AmbientSource;
    public AudioClip[] MusicTracks;
    public AudioSource MusicSource;
    

    public AudioMixer Mixer;
    private AudioMixerSnapshot MusicUp, MusicDown;
    //private AudioMixerSnapshot AmbientUp, AmbientDown;

    public bool MusicPlaying { get; internal set; }

    private void Awake()
    {
        if (I)
        {
            Destroy(this);
            return;
        }

        DontDestroyOnLoad(gameObject);
        I = this;

        MusicUp = Mixer.FindSnapshot("MusicUp");
        MusicDown = Mixer.FindSnapshot("MusicDown");
        //AmbientUp = Mixer.FindSnapshot("AmbientUp");
        //AmbientDown = Mixer.FindSnapshot("AmbientDown");
    }

    /*public void StartAmbientTrack()
    {
        MusicPlaying = false;
        trackIndex = -1;
        AmbientVolumeInc(3f);
        StartClip(AmbientTrack, AmbientSource);
    }*/

    public void PlayMusicTrack(int index)
    {
        if (trackIndex == index) return;
        MusicPlaying = true;
        trackIndex = index;
        MusicVolumeInc(1f);
        StartClip(MusicTracks[index], MusicSource);
    }

    public void StopMusicTrack()
    {
        trackIndex = -1;
        MusicPlaying = false;
        MusicVolumeDec(5f);
    }

    void StartClip(AudioClip clip, AudioSource source)
    {
        source.loop = true;
        source.clip = clip;
        source.Play();
    }

    public void MusicVolumeInc(float time)
    {
        MusicUp.TransitionTo(time);
    }

    public void MusicVolumeDec(float time)
    {
        MusicDown.TransitionTo(time);
    }
    /*
    public void AmbientVolumeInc(float time)
    {
        AmbientUp.TransitionTo(time);
    }

    public void AmbientVolumeDec(float time)
    {
        AmbientDown.TransitionTo(time);
    }*/

    public void SetMusicVolumeFull()
    {
        MusicUp.TransitionTo(0);
    }
}
