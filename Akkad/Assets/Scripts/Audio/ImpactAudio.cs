﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ImpactAudio : MonoBehaviour 
{
    #region vars
    public AudioPlayer Audio;
    public int channel = 0;
    #endregion
    #region init
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events
    private void OnTriggerEnter(Collider collider)
    {
        Audio.Play3DSound(channel);
    }
#endregion
}
