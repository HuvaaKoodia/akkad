﻿using System.Collections;
using UnityEngine;

public class Lvl4Controller : MonoBehaviour
{
    #region vars
    public static Lvl4Controller I;

    public float PlayerJumpImpulse = 10f;
    public float PlayerJumpForce = 10f;
    public float PlayerJumpForceReduction = 0.5f;
    public LookAtScript AncientLookAt;

    public Material Skybox;
    public AnimationCurve SkyboxCurve;
    #endregion
    #region init
    private void Awake()
    {
        I = this;
    }

    private void Start()
    {
        Physics.gravity = Vector3.zero;

        var player = GameController.I.Player;

        player.UseFallDamage = false;
        player.CameraVerticalLookBounds.x = -player.CameraVerticalLookBounds.y;

        player.MovementSystem.JumpImpulse = PlayerJumpImpulse;
        player.MovementSystem.JumpForce = PlayerJumpForce;
        player.MovementSystem.JumpForceReductionMultiplier = PlayerJumpForceReduction;

        StartCoroutine(SkyboxCoroutine());

        MusicController.I.MusicVolumeDec(1f);

        AncientLookAt.Target = GameController.I.Player.transform;
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    #endregion
    #region private interface
    private IEnumerator SkyboxCoroutine()
    {
        float start = 2000;
        float end = 70;
        float duration = 3f;

        Skybox.SetFloat("_Saturation", start);

        yield return new WaitForSeconds(2f);

        float startTime = Time.time;

        while (true)
        {
            yield return null;

            float percent = SkyboxCurve.Evaluate((Time.time - startTime) / duration);
            Skybox.SetFloat("_Saturation", Mathf.Lerp(start, end, percent));

            if (percent > 1) break;
        }

        Skybox.SetFloat("_Saturation", end);
    }
    #endregion
    #region events
    #endregion
}
