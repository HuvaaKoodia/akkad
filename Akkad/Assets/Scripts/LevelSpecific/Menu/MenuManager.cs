﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour {

    public GameObject MainMenu;
    public GameObject OptionsMenu;
    public GameObject Credits;
    public GameObject ContinueButton;

    public void Start()
    {
        ContinueButton.SetActive(SaveController.HasSavedLevel());

        MusicController.I.PlayMusicTrack(5);
    }

    public void StartNewGame()
    {
        GameController.LoadLevel(1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ContinueGame()
    {
        SaveController.LoadSavedLevel();
    }

    public void OpenMainMenu()
    {
        TurnOffPanels();
        MainMenu.SetActive(true);
    }
    
    public void OpenOptionsMenu()
    {
        TurnOffPanels();
        OptionsMenu.SetActive(true);
    }

    public void OpenCredits()
    {
        TurnOffPanels();
        Credits.SetActive(true);
    }

    private void TurnOffPanels()
    {
        MainMenu.SetActive(false);
        OptionsMenu.SetActive(false);
        Credits.SetActive(false);
    }

}
