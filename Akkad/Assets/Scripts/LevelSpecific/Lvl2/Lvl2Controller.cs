﻿using UnityEngine;
using System.Collections.Generic;
public class Lvl2Controller : MonoBehaviour
{
#region vars
    enum LevelState
    {
        KurConvo = 0,
        EndLevel = 1
    }
    private int currentConvo = 0;
    private List<string> Conversations = new List<string>();
    private bool KurConvo;
    
    public static Lvl2Controller I;
    public DialogTrigger DialogTriggerAncient;
    public GameObject Lvl2Door;
    LevelState Lvl2;

    //conversations
    public string KurIntro;
    public string KurOutro;

    //private int levelState = 0;
#endregion
#region init
	private void Awake()
	{
		I = this;
	}

    private void Start()
    {
        Conversations.Add(KurIntro);
        Conversations.Add(KurOutro);
        Lvl2 = LevelState.KurConvo;
        Lvl2Door.SetActive(false);
        DialogController.I.OnDialogEndEvent += OnDialogEndEvent;

        MusicController.I.PlayMusicTrack(1);
    }

    #endregion
    #region logic
    public void UpdateState()

    {
        currentConvo++;
        Lvl2++;
        Debug.Log(Lvl2);
        Debug.Log("Current convo = " + currentConvo);
        UpdateConvo(Conversations[currentConvo]);

    }

    #endregion
    #region public interface
    #endregion
    #region private interface
    private void UpdateConvo(string DialogName)
    {
        DialogTriggerAncient.UpdateDialogName(DialogName);
    }
    #endregion
    #region events

    private void OnDialogEndEvent(string dialogName)
    {
        if (dialogName == KurOutro)
        {
            UpdateState();
            OpenPortal();
        }
    }

    private void OpenPortal()
    {
        Lvl2Door.SetActive(true);
    }

    #endregion
}
