﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Lvl3Controller : MonoBehaviour
{

    #region vars
    enum LevelState
    {
        MaConvo = 0,
        Maze = 1,
        EndLvlConvo = 2
    }

    public static Lvl3Controller Lvl3;
    public GameObject Lvl3Door;

    public string MaConvo;
    public string Maze;
    public string EndLevelConvo;

    //public string EndConvo;
    private static LevelState Lvl;


    #endregion
    #region init
    private PlayerView player;
    public static Lvl3Controller lvl3;

    private void Awake()
    {
        lvl3 = this;
    }

    private void Start()
    {
        player = GameController.I.Player;
        player.UseFallDamage = false;
        Lvl = LevelState.MaConvo;

        DialogController.I.OnDialogStartEvent += I_OnStartDialogEvent;

        MusicController.I.PlayMusicTrack(2);
    }


    #endregion
    #region logic

    public void OpenPortal()
    {
        UpdateState();
        Lvl3Door.SetActive(true);
    }

    public void UpdateState()
    {
        Lvl++;
        Debug.Log(Lvl);
    }
    #endregion
    #region public interface
    #endregion
    #region private interface
    #endregion
    #region events

    private void I_OnStartDialogEvent(string text)
    {
        if (text == MaConvo)
        {
            UpdateState();
        }

    }
    #endregion
}
