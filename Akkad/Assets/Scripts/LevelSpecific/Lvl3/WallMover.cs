﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class WallMover : MonoBehaviour
{

    enum Direction { Center, Right, Left, Up, Down }

    Direction currentDirection;

    public SplineWalker WallToMove;
    public BezierSpline[] Splines;
    public bool active;
    public bool UpAndDown;
    public bool OnlyLeft;
    public bool OnlyRight;
    public int ChanceToBlock;
    public float MinDurationStay;
    public float MaxDurationStay;
    public float duration;

    void Start()
    {

        WallToMove.spline = null;
        if(duration > 0) WallToMove.duration = duration;

        StartCoroutine("testMoving");
    }

    IEnumerator testMoving()
    {
        while (active)
        {
            float time = Random.Range(MinDurationStay,MaxDurationStay);
            yield return new WaitForSeconds(time);
            if (OnlyLeft)
            {
                int random = Random.Range(0, 10);
                if (random >= ChanceToBlock)
                {
                    Move(Direction.Left);
                    yield return new WaitForSeconds(time);
                    Move(Direction.Center);
                }
                else
                {
                    continue;
                }


            }else if (OnlyRight)
            {
                int random = Random.Range(0, 10);
                if (random >= ChanceToBlock)
                {
                    Move(Direction.Right);
                    yield return new WaitForSeconds(time);
                    Move(Direction.Center);
                }
                else
                {
                    continue;
                }

            }
            else
            {
                Direction TempDirection = (Direction)Random.Range(1, 3); 
                int random = Random.Range(0, 10);
                if (random >= ChanceToBlock)
                {
                    Move(TempDirection);
                    yield return new WaitForSeconds(time);
                    Move(Direction.Center);
                }
                else
                {
                    continue;
                }
            }



        }
    }

    void Move(Direction direction)
    {
        //Debug.Log(direction);
        WallToMove.moving = true;
        WallToMove.goingForward = true;

        currentDirection = direction;
        if (currentDirection == Direction.Center)
        {
            WallToMove.goingForward = false;
            return;
        }
        if (direction == Direction.Left)
        {
            WallToMove.spline = Splines[1];

        }
        else if (direction == Direction.Right)
        {

            WallToMove.spline = Splines[0];
        }
        else if (direction == Direction.Up)
        {
            WallToMove.spline = Splines[2];
        }
        else if (direction == Direction.Down)
        {
            WallToMove.spline = Splines[3];
        }

    }


}
