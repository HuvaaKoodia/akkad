﻿using UnityEngine;
using System.Collections;

public class SpikeTraps : MonoBehaviour {

    public float delayTime;
    public Animator Animator;
    void Start()
    {
        StartCoroutine("StartSpikes", delayTime);
    }
    
	IEnumerator StartSpikes(float time)
    {
        while (true)
        {
            yield return new WaitForSeconds(time);
            Animator.SetTrigger("Spike");
            yield return new WaitForSeconds(1);
        }
    }
}
