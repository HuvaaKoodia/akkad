﻿using UnityEngine;
using UnityEngine.Events;

public class ManageCollectables : MonoBehaviour
{
    private int allCollectables;
    private int collectedCollectables;
    public UnityEvent ObjectiveComplete, OnOrbCollected;
    public int AmountToCollect = 4;

    void Start()
    {
        allCollectables = transform.childCount;

        Debug.Log(allCollectables);
    }

    public void CheckCollectedCollectables()
    {
        OnOrbCollected.Invoke();

        collectedCollectables++;
        Debug.Log(collectedCollectables);
        if (collectedCollectables == AmountToCollect)
        {
            Debug.Log("Objective Complete!");
            ObjectiveComplete.Invoke();
        }
    }
}
