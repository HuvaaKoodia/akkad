﻿using UnityEngine;
using System.Collections;

public class PillarActivate : MonoBehaviour {

	public bool CanBeActivated = false; 
	public bool isActivated = false;
	public ParticleSystem myParticles;
	public event Delegates.Action OnActivatedEvent; 

	// Use this for initialization
	void Start () {
	
	}


	public void ActivatePillar()
	{
		if (CanBeActivated) {
			if(OnActivatedEvent != null)OnActivatedEvent ();

			isActivated = true;
			myParticles.Play ();
            Debug.Log(this.name + " is playing particles");
		}
	}
}
