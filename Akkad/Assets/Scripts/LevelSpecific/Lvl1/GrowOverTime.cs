﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GrowOverTime : MonoBehaviour 
{
    #region vars
    public float Duration = 1f;
    public Vector3 StartScale = Vector3.one, EndScale = Vector3.one * 2;
    public bool PlayOnStart = true;
    #endregion
    #region init
    private void Start()
    {
        if (PlayOnStart) Grow();
    }
    #endregion
    #region logic
    #endregion
    #region public interface
    public void Grow()
    {
        StartCoroutine(GrowCoroutine());
    }
    #endregion
    #region private interface
    private IEnumerator GrowCoroutine()
    {
        float percent = 0;
        float speed = 1 / Duration;

        while (true)
        {
            percent += Time.deltaTime * speed;

            transform.localScale = Vector3.Lerp(StartScale, EndScale, percent);
            if (percent >= 1) break;

            yield return null;
        }
    }
    #endregion
    #region events
    #endregion
}
