﻿using UnityEngine;
using System.Collections;
using InteractionSystem;
using UnityEngine.Events;


public class Collectable : MonoBehaviour
{
    public UnityEvent OnPressedEvent;
    public bool HasBeenCollected;
    public Collider Collider;
    public ParticleSystem PS, PickupPS;
    public AudioPlayer Audio;

    private void OnTriggerEnter(Collider collider)
    {
        OnPressedEvent.Invoke();
        Collider.enabled = false;
        PS.Stop();
        PickupPS.Play();
        Audio.Play3DSound(1);
    }
}
