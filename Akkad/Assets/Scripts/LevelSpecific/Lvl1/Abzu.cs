﻿using UnityEngine;
using System.Collections;
using InteractionSystem;
using System.Collections.Generic;

public class Abzu : MonoBehaviour {


    #region vars
    public InteractionLayer InteractionLayer;
    public string Introduction, Mission, EndTalk, PepTalk;
    public bool IntroductionLoop;
    public bool Skipped;
    public float WaitTime;

    private List<string> conversation = new List<string>();

    #endregion
    #region init
    void Start()
    {
        conversation.Add(Introduction);
        conversation.Add(Mission);
        conversation.Add(EndTalk);

    }
    #endregion
    #region logic
    #endregion
    #region public interface
    IEnumerator TextLoop()
    {
        if (IntroductionLoop)
        {
            for(int i = 0; i < conversation.Count; i++)
            {
                if (!Skipped)
                {
                    InteractionLayer.InteractionMessage = conversation[i];
                    yield return new WaitForSeconds(WaitTime);
                }
                Skipped = false;
                i++;
            }
        }
        else
        {
            InteractionLayer.InteractionMessage = PepTalk;
            yield return new WaitForSeconds(WaitTime);
        }
        yield return null;
    }
    #endregion
    #region private interface


    #endregion
    #region events

    #endregion
}
