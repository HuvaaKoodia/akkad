﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ManagePillars : MonoBehaviour
{

	public List<PillarActivate> Pillars = new List<PillarActivate> ();
	private int currentPillar = 0;

	// Use this for initialization
	void Start ()
	{

		if (Pillars.Count == 0) {
			foreach (Transform child in transform) {
				Pillars.Add (child.gameObject.GetComponent<PillarActivate> ());
			}		
		}

		if (Pillars.Count >= 0) {
			Pillars [currentPillar].OnActivatedEvent += NextPillar;
			Pillars [currentPillar].CanBeActivated = true;
		}
	}


	void NextPillar ()
	{
		Pillars [currentPillar].CanBeActivated = false;
		Pillars [currentPillar].OnActivatedEvent -= NextPillar;
		currentPillar++;
		if (currentPillar < Pillars.Count) {
			Pillars [currentPillar].CanBeActivated = true;
			Pillars [currentPillar].OnActivatedEvent += NextPillar;
		}

	}
}
