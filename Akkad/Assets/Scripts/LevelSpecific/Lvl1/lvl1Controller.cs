﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class lvl1Controller : MonoBehaviour {

	#region vars

    public static lvl1Controller I;

	public GameObject Abzu;
    public GameObject LightOrbs;
    public GameObject Portal;
    public Transform PortalPosition;
    public ManageMushrooms ManageMushrooms;

    public System.Action OnOrbCollectedEvent;

    public string IntroDialogName, AbzuIntroDialogName, MissionDialogName, EndDialogName;

    private int currentConvo;
    private PlayerView player;
    private List<string> Conversations = new List<string>();

    #endregion
    #region init

    private void Awake()
	{
        I = this;   

        Conversations.Add(IntroDialogName);
        Conversations.Add(AbzuIntroDialogName);
        Conversations.Add(MissionDialogName);
        Conversations.Add(EndDialogName);
    }

	private void Start()
	{
        player = GameController.I.Player;
        player.UseFallDamage = false;

        DialogController.I.OnDialogEndEvent += OnDialogEnd;

        
        MusicController.I.PlayMusicTrack(0);
    }

    #endregion
    #region logic

    public void OrbCollected()
    {
        if (OnOrbCollectedEvent != null) OnOrbCollectedEvent();
    }

    public void CallAbzu()
    {
        StartCoroutine("AbzuAwakens");
    }

    public void OpenPortal()
    {
        Portal.SetActive(true);
        Portal.transform.position = PortalPosition.position;
        Portal.transform.rotation = PortalPosition.rotation;

    }

    public void EnableLightOrbs()
    {
        LightOrbs.SetActive(true);
    }

    public void EnableMushroomInteraction()
    {
        ManageMushrooms.EnableInteraction();
    }
    
    public void UpdateState()
    {
        currentConvo++;
    }

    IEnumerator AbzuAwakens()
	{
        Abzu.GetComponent<LookAtScript>().Target = player.transform;
        Abzu.GetComponentInChildren<Animator>().SetBool("isAwoken", true);
        yield return null;

    }
    #endregion
    #region public interface
        public void PlayNextDialog()
        {
            DialogController.I.StartDialog(Conversations[currentConvo]);
        }
        
    #endregion
    #region private interface
    #endregion
    #region events

    private void OnDialogEnd(string text)
    {
        if (text == MissionDialogName)
        {
            //don't update state
            return;
        }

        if (text == AbzuIntroDialogName)
        {
            EnableLightOrbs();
            GUIController.I.ShowControls(30f);
        }

        if(text == EndDialogName)
        {
            OpenPortal();
        }

        UpdateState();
    }
    #endregion
}
