﻿using UnityEngine;
using System.Collections;

public class CallAbzu : MonoBehaviour {

    public bool HasEnteredOnce = false;

	// Use this for initialization
	void Start () {
        
	}
	
	void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player" && !HasEnteredOnce)
        {
            lvl1Controller.I.CallAbzu();
            HasEnteredOnce = true;
        }
    }
}
