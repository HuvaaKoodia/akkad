﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Cameras;

public class Lvl1StartCutscene : MonoBehaviour 
{
    #region vars
    public SplineWalker AbzuRevealSplineWalker;
    public Transform AbzuLookTarget;
#endregion
#region init
	private IEnumerator Start () 
	{
        var player = GameController.I.Player;
        var camera = CameraController.I.Camera;

        //disable player and camera
        

        camera.enabled = false;
        CameraController.I.CameraRig.enabled = false;

        camera.transform.position = AbzuRevealSplineWalker.spline.GetPoint(0);
        camera.transform.rotation = Quaternion.LookRotation(camera.transform.position.To(player.transform.position + player.CenterPointOffset));

        yield return null; //wait one frame to make sure the player input is disabled properly
        GameController.I.DisablePlayerMovement();

        yield return new WaitForSeconds(2f);

        //intro dialog
        lvl1Controller.I.PlayNextDialog();

        while (DialogController.I.DialogOn) yield return null;

        //GameController enables player movement after dialog so lets disable it again! HACK HACK!
        GameController.I.DisablePlayerMovement();

        //abzu reveal spline movement and abzu intro dialog
        AbzuRevealSplineWalker.goingForward = true;
        lvl1Controller.I.CallAbzu();

        while (AbzuRevealSplineWalker.goingForward)
        {
            camera.transform.position = AbzuRevealSplineWalker.transform.position;
            camera.transform.rotation = Quaternion.Lerp(camera.transform.rotation, Quaternion.LookRotation(camera.transform.To(AbzuLookTarget), Vector3.up), Time.deltaTime * 2);

            yield return null;
        }

        lvl1Controller.I.PlayNextDialog();

        while (DialogController.I.DialogOn) yield return null;

        //reenable camera (player is enabled by GameController)
        camera.enabled = true;
        CameraController.I.CameraRig.enabled = true;
    }
#endregion
#region logic
#endregion
#region public interface
#endregion
#region private interface
#endregion
#region events
#endregion
}
