﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;

public class ManageMushrooms : MonoBehaviour {

    public bool AlreadyEnabled;
    public UnityEvent ObjectiveComplete;
    public List<Collectable> Mushrooms = new List<Collectable>();

    public void Start()
    {
        foreach(Transform child in transform)
        {
            Mushrooms.Add(child.GetComponent<Collectable>());
        }
    }
    public void EnableInteraction()
    {
        if (!AlreadyEnabled)
        {
            for(int i = 0; i < Mushrooms.Count; i++)
            {
                //child.gameObject.layer = LayerMask.NameToLayer("Interactable");
                //Mushrooms[i].gameObject.layer = LayerMask.NameToLayer("Interactable");
                Mushrooms[i].enabled = true;
            }
        }
        AlreadyEnabled = true;
    }

    public void MissionComplete()
    {
        Debug.Log("You've got a mushroom");
        ObjectiveComplete.Invoke();
    }
}
